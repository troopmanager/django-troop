For developers of django-troop

Please Read the Wiki for style guide and guide to the codebase.

Vision - create an open-source, free, self-hostable equivalent to commercial BSA management software like Troopwebhost, Troopmaster and Scoutbook.

Installation -

Docker and Docker-compose

On Windows - you'll need to install Windows Subsystem for Linux (WSL), then Docker Desktop.

From the django-troop/django_troop directory, you can start the project in a testing, one-troop mode with

docker-compose up -d

On Mac OS or Linux, install Just as a general script enabler. With just installed, from django-troop/django_troop

just up -d

will run the project.

Open the project in a browser on localhost:8000

The default one-troop login/password is admin/admin.

A default troop of data is available at data/example_troop_scoutbook/advancement.csv and ...../personal.csv

See the Issues on gitlab for the project for suggested things to work on.
