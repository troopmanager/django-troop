DOCKER_COMPOSE := "docker-compose"
DOCKER_COMPOSE_FILE := "docker-compose.yml"
TROOP_DOCKER_COMPOSE_FILE := "troop-docker-compose.yml"

CLUSTER_NAME := "troopmanager-dev"
CLUSTER_CONFIG := "deploy/infra/ctlptl/*.yaml"
DEV_CONTEXT := env("DEV_CONTEXT", "kind-troopmanager-dev")


# Show available recipes
[group('just')]
default:
	@just --list

# Start tunnel from remote server
[group('infra')]
ssh-tunnel PORT REMOTE_SERVER LOCAL_SERVER:
	ssh -f -N -L {{PORT}}:{{LOCAL_SERVER}}:{{PORT}} {{REMOTE_SERVER}}

# Open a shell in the TROOP docker container
[group('infra')]
shell TROOP *ARGS="bash":
	kubectl --context={{DEV_CONTEXT}} -n troop-{{TROOP}} exec -it deployment/django-troop -c app -- {{ARGS}}

[group('infra')]
redis:
	kubectl --context={{DEV_CONTEXT}} -n redis exec -it redis-master-0 -- redis-cli

# Apply host entries to /etc/hosts which will map the current set of troops.
[group('infra')]
apply-hosts:
	./scripts/apply_hosts.py

# Create a new superuser
[group('config')]
createsuperuser TROOP USERNAME EMAIL:
	@just shell {{TROOP}} python manage.py createsuperuser --username {{USERNAME}} --email {{EMAIL}}

# Create a new superuser
[group('config')]
logs TROOP *ARGS:
	kubectl --context={{DEV_CONTEXT}} -n troop-{{TROOP}} logs -l app=django-troop {{ARGS}}

# Create a new troop's files
[group('config')]
create TROOP PORT:
	scripts/setup_troop.sh {{TROOP}} {{PORT}}

# Manage a troop with args
[group('config')]
manage TROOP *ARGS:
	@just shell {{TROOP}} python manage.py {{ARGS}}

[group('repo')]
test *ARGS:
	#!/usr/bin/env bash
	set -euxo pipefail
	cd django_troop
	# ENV=test must be provided directly in the env so that django knows to load
	# test.env.
	export ENV=test
	python manage.py collectstatic --noinput
	python manage.py test {{ARGS}}

# Install the project pre-commit hooks.
[group('repo')]
install-pre-commit:
	pre-commit install -f

# Run the project pre-commit hooks.
[group('repo')]
run-pre-commit *ARGS: install-pre-commit
	pre-commit run --all-files --hook-stage pre-commit {{ARGS}}

# Run the project pre-push hooks.
[group('repo')]
run-pre-push *ARGS: install-pre-commit
	pre-commit run --all-files --hook-stage pre-push {{ARGS}}

# Run the project pre-commit hooks.
[group('repo')]
lint:
	@just run-pre-commit

# Create the dev cluster, the cluster already exists, it will apply the configuration again.
[group('k8s')]
create-dev-cluster:
	ctlptl apply -f {{CLUSTER_CONFIG}}

# Ensure the dev cluster exists and is ready, and run tilt to automatically deploy and monitor resources.
[group('k8s')]
run-dev-cluster: create-dev-cluster
	tilt up

# Block until the dev cluster is ready.
[group('k8s')]
wait-dev-cluster:
	kubectl wait --for=condition=Ready node --all --timeout=1m

# Destroy the dev cluster.
[group('k8s')]
delete-dev-cluster:
	ctlptl delete -f {{CLUSTER_CONFIG}}
