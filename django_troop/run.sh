#!/bin/bash

set -euxo pipefail

echo "starting gunicorn"
gunicorn django_troop.wsgi --bind 0.0.0.0:8000 --reload --timeout 1800
code=$?
sleep 1
echo "gunicorn exited with code $code"
sleep 1
exit $code
