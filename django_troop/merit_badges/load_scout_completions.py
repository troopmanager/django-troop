import csv
import logging
from datetime import datetime
from pathlib import Path

from badges.models import Badge, get_mb_name
from django.conf import settings
from django.core.management.base import BaseCommand
from requirements.models import Requirement
from scouts.models import User
from signoffs.models import Signoff
from tqdm import tqdm

DATA_DIR = settings.DATA_DIR

logger = logging.getLogger(__name__)


def load_merit_badges(path, load_type):
    match load_type:
        case "troopwebhost":
            with open(path / "merit_badges.csv", encoding="utf-8-sig") as f:
                number = len(f.readlines())
                f.seek(0)
                reader = csv.DictReader(f)
                for line in tqdm(reader, desc="Merit Badge Completions", total=number):
                    load_troopwebhost_merit_badges(line)
        case "troopmaster":
            pass
        case "scoutbook":
            pass


def load_troopwebhost_merit_badges(line):
    ic()  # noqa
    if line["Earned"]:
        merit_badge_name = get_mb_name(line["Merit Badge"])
        logger.info(f"processing merit badge {merit_badge_name}")
        badge, created = Badge.objects.get_or_create(
            badge_type="merit_badge", name=merit_badge_name
        )

        name = User.name_from_string(line["Scout"])
        scout = User.scouts.get(
            last_name=name.get("last_name"),
            first_name=name.get("first_name"),
        )
        logger.info(f"for scout {scout.last_name}, {scout.first_name}")

        try:
            requirement = Requirement.objects.get(badge=badge, code="Completion")
        except Requirement.DoesNotExist:
            logger.warn(f"Warning: Non-standard MB created: {badge.name}")
            requirement = Requirement.objects.create(badge=badge, code="Completion")
        signoff, created = Signoff.objects.get_or_create(
            scout=scout, requirement=requirement
        )

        signoff.date = datetime.strptime(line["Earned"], "%m/%d/%y").date()
        signoff.signed = True
        signoff.save()
        logger.info(f"scout {scout} signed off for {badge} on {signoff.date}")


def load_troopmaster_merit_badges(scout):
    pass


class Command(BaseCommand):
    help = "Loads Merit Badges"

    def add_arguments(self, parser):
        parser.add_argument("config_dir", nargs="?", default=DATA_DIR, type=str)

    def handle(self, *args, **kwargs):
        DATA_DIR = Path(kwargs["config_dir"])

        load_merit_badges(DATA_DIR, load_type=kwargs["load_type"])


def main(*args, **kwargs):
    Command().handle(config_dir=DATA_DIR, **kwargs)
