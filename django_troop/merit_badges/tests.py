# Create your tests here.
import logging
import sys

import yaml
from django.core.management import call_command
from django.test import TestCase
from icecream import ic
from merit_badges.utils import (
    get_eagle_merit_badges,
    get_life_merit_badges,
    get_star_merit_badges,
)
from scouts.load_scouts import load_scout
from scouts.models import User
from signoffs.models import Signoff

logger = logging.getLogger(__name__)

stream_handler = logging.StreamHandler(stream=sys.stdout)
logger.addHandler(stream_handler)


class EarlyMiddleMeritBadgeImportTests(TestCase):
    def setUp(self):
        call_command("load_base")
        with open("data/example_scouts/early_middle_scout.yaml") as f:
            self.file = yaml.safe_load(f)
            for name in self.file.values():
                load_scout(name)
            self.scout = User.objects.get(last_name="Larsen", first_name="Reggie")
            ic("setting up the scout")
            ic(self.scout)

    def test_early_middle_star_merit_badges(self):
        badges = Signoff.objects.filter(
            scout=self.scout,
            requirement__badge__badge_type="merit_badge",
            requirement__code="Completion",
        )

        eagle_badges, other_badges, complete = get_star_merit_badges(badges)

        self.assertEqual(len(eagle_badges), 0)
        self.assertEqual(len(other_badges), 2)

        self.assertEqual(other_badges[0].requirement.badge.name, "Art")
        self.assertEqual(other_badges[1].requirement.badge.name, "Sculpture")
        self.assertFalse(complete)

    def test_early_middle_life_merit_badges(self):
        badges = Signoff.objects.filter(
            scout=self.scout,
            requirement__badge__badge_type="merit_badge",
            requirement__code="Completion",
        )

        eagle_badges, other_badges, complete = get_life_merit_badges(badges)

        self.assertEqual(len(eagle_badges), 0)
        self.assertEqual(len(other_badges), 2)
        self.assertFalse(complete)

    def test_early_middle_eagle_merit_badges(self):
        badges = Signoff.objects.filter(
            scout=self.scout,
            requirement__badge__badge_type="merit_badge",
            requirement__code="Completion",
        )

        ic(badges)

        eagle_badges, rest, complete = get_eagle_merit_badges(badges)

        print(eagle_badges, rest, complete)

        self.assertEqual(len(eagle_badges), 4)
        self.assertEqual(len(rest), 0)
        self.assertFalse(complete)

        self.assertEqual(eagle_badges["03o"].requirement.badge.name, "Art")
        self.assertEqual(eagle_badges["03p"].requirement.badge.name, "Sculpture")
        self.assertEqual(eagle_badges["03q"].requirement.badge.name, "Theater")
        self.assertEqual(eagle_badges["03r"].requirement.badge.name, "Public Speaking")


class MiddleMeritBadgeImportTest(TestCase):
    def setUp(self):
        call_command("load_base")
        with open("data/example_scouts/middle_scout.yaml") as f:
            self.file = yaml.safe_load(f)
            for name in self.file.values():
                load_scout(name)
            self.scout = User.objects.get(last_name="Ray", first_name="Hollie")

    def test_middle_star_merit_badges(self):
        badges = Signoff.objects.filter(
            scout=self.scout,
            requirement__badge__badge_type="merit_badge",
            requirement__code="Completion",
        )

        eagle_badges, other_badges, complete = get_star_merit_badges(badges)

        self.assertEqual(len(eagle_badges), 2)
        self.assertEqual(len(other_badges), 2)

        self.assertEqual(eagle_badges[0].requirement.badge.name, "Swimming")
        self.assertEqual(eagle_badges[1].requirement.badge.name, "First Aid")
        self.assertEqual(other_badges[0].requirement.badge.name, "Canoeing")
        self.assertEqual(other_badges[1].requirement.badge.name, "Kayaking")
        self.assertFalse(complete)

    def test_middle_life_merit_badges(self):
        badges = Signoff.objects.filter(
            scout=self.scout,
            requirement__badge__badge_type="merit_badge",
            requirement__code="Completion",
        )

        eagle_badges, other_badges, complete = get_life_merit_badges(badges)

        self.assertEqual(len(eagle_badges), 0)
        self.assertEqual(len(other_badges), 1)
        self.assertEqual(other_badges[0].requirement.badge.name, "Wood Carving")
        self.assertFalse(complete)

    def test_middle_eagle_merit_badges(self):
        badges = Signoff.objects.filter(
            scout=self.scout,
            requirement__badge__badge_type="merit_badge",
            requirement__code="Completion",
        )

        eagle_badges, rest, complete = get_eagle_merit_badges(badges)

        self.assertEqual(len(eagle_badges), 5)
        self.assertEqual(len(rest), 0)
        self.assertFalse(complete)

        self.assertEqual(eagle_badges["03a"].requirement.badge.name, "First Aid")
        self.assertEqual(eagle_badges["03l"].requirement.badge.name, "Swimming")
        self.assertEqual(eagle_badges["03o"].requirement.badge.name, "Canoeing")
        self.assertEqual(eagle_badges["03p"].requirement.badge.name, "Kayaking")
        self.assertEqual(eagle_badges["03q"].requirement.badge.name, "Wood Carving")


class OlderMeritBadgeImportTest(TestCase):
    def setUp(self):
        call_command("load_base")
        with open("data/example_scouts/older_scout.yaml") as f:
            self.file = yaml.safe_load(f)
            for name in self.file.values():
                load_scout(name)
            self.scout = User.objects.get(last_name="Santo", first_name="Conrad")

    def test_older_star_merit_badges(self):
        badges = Signoff.objects.filter(
            scout=self.scout,
            requirement__badge__badge_type="merit_badge",
            requirement__code="Completion",
        )

        eagle_badges, other_badges, complete = get_star_merit_badges(badges)

        self.assertEqual(len(eagle_badges), 4)
        self.assertEqual(len(other_badges), 2)

        self.assertEqual(eagle_badges[0].requirement.badge.name, "Swimming")
        self.assertEqual(
            eagle_badges[1].requirement.badge.name, "Environmental Science"
        )
        self.assertEqual(eagle_badges[2].requirement.badge.name, "First Aid")
        self.assertEqual(
            eagle_badges[3].requirement.badge.name, "Citizenship in the Community"
        )
        self.assertEqual(other_badges[0].requirement.badge.name, "Leatherwork")
        self.assertEqual(other_badges[1].requirement.badge.name, "Mammal Study")
        self.assertTrue(complete)

    def test_older_life_merit_badges(self):
        badges = Signoff.objects.filter(
            scout=self.scout,
            requirement__badge__badge_type="merit_badge",
            requirement__code="Completion",
        )

        eagle_badges, other_badges, complete = get_life_merit_badges(badges)

        self.assertEqual(len(eagle_badges), 3)
        self.assertEqual(len(other_badges), 2)
        self.assertEqual(eagle_badges[0].requirement.badge.name, "Personal Management")
        self.assertEqual(
            eagle_badges[1].requirement.badge.name, "Citizenship in the Nation"
        )
        self.assertEqual(
            eagle_badges[2].requirement.badge.name, "Citizenship in the World"
        )
        self.assertEqual(other_badges[0].requirement.badge.name, "Game Design")
        self.assertEqual(other_badges[1].requirement.badge.name, "Fishing")
        self.assertTrue(complete)

    def test_older_eagle_merit_badges(self):
        badges = Signoff.objects.filter(
            scout=self.scout,
            requirement__badge__badge_type="merit_badge",
            requirement__code="Completion",
        )

        eagle_badges, rest, complete = get_eagle_merit_badges(badges)

        self.assertEqual(len(eagle_badges), 21)
        self.assertEqual(len(rest), 5)
        self.assertTrue(complete)

        self.assertEqual(eagle_badges["03a"].requirement.badge.name, "First Aid")
        self.assertEqual(
            eagle_badges["03b"].requirement.badge.name, "Citizenship in the Community"
        )
        self.assertEqual(
            eagle_badges["03c"].requirement.badge.name, "Citizenship in the Nation"
        )
        self.assertEqual(
            eagle_badges["03d"].requirement.badge.name, "Citizenship in Society"
        )
        self.assertEqual(
            eagle_badges["03e"].requirement.badge.name, "Citizenship in the World"
        )
        self.assertEqual(eagle_badges["03f"].requirement.badge.name, "Communication")
        self.assertEqual(eagle_badges["03g"].requirement.badge.name, "Cooking")
        self.assertEqual(eagle_badges["03h"].requirement.badge.name, "Personal Fitness")
        self.assertEqual(
            eagle_badges["03i"].requirement.badge.name, "Emergency Preparedness"
        )
        self.assertEqual(
            eagle_badges["03j"].requirement.badge.name, "Environmental Science"
        )
        self.assertEqual(
            eagle_badges["03k"].requirement.badge.name, "Personal Management"
        )
        self.assertEqual(eagle_badges["03l"].requirement.badge.name, "Swimming")
        self.assertEqual(eagle_badges["03m"].requirement.badge.name, "Camping")
        self.assertEqual(eagle_badges["03n"].requirement.badge.name, "Family Life")
        self.assertEqual(eagle_badges["03o"].requirement.badge.name, "Leatherwork")
        self.assertEqual(eagle_badges["03p"].requirement.badge.name, "Mammal Study")
        self.assertEqual(eagle_badges["03q"].requirement.badge.name, "Game Design")
        self.assertEqual(eagle_badges["03r"].requirement.badge.name, "Fishing")
        self.assertEqual(eagle_badges["03s"].requirement.badge.name, "Woodwork")
        self.assertEqual(eagle_badges["03t"].requirement.badge.name, "Geocaching")
        self.assertEqual(eagle_badges["03u"].requirement.badge.name, "Photography")

        self.assertEqual(rest[0].requirement.badge.name, "Gardening")
        self.assertEqual(rest[1].requirement.badge.name, "Wood Carving")
        self.assertEqual(rest[2].requirement.badge.name, "Chess")
        self.assertEqual(rest[3].requirement.badge.name, "Climbing")
        self.assertEqual(rest[4].requirement.badge.name, "Astronomy")
