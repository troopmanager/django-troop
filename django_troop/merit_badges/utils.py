import copy
from collections import defaultdict
from itertools import chain

from django.core.cache import cache
from requirements.models import Requirement


def get_star_merit_badges(badge_signoff_list, num_eagle=4, num_other=2):
    """from a list of merit badge signoffs, return the first four eagle-required
    merit badges, plus a list of the next two badges earned"""

    if not badge_signoff_list:
        return [], [], False

    """get the list of eagle required badges, sort by date"""
    eagle_badges = (
        sorted(
            filter(lambda x: x.requirement.badge.eagle_required, badge_signoff_list),
            key=lambda x: (x.date, x.requirement.badge.name),
        )
        or []
    )

    non_eagle_badges = (
        sorted(
            filter(
                lambda x: not x.requirement.badge.eagle_required, badge_signoff_list
            ),
            key=lambda x: (x.date, x.requirement.badge.name),
        )
        or []
    )

    remaining_eagle_badges = eagle_badges[num_eagle:] or []

    """combine the list of non-eagle badges and the eagle badges beyond what are needed for this rank, sort the combined list, and assign the first num_other badges"""
    other_badges = (
        sorted(
            chain(non_eagle_badges, remaining_eagle_badges),
            key=lambda x: (x.date, x.requirement.badge.name),
        )[:num_other]
        or []
    )

    """the eagle badges are the first num_eagle badges in the sorted eagle_badges list"""
    eagle_badges = eagle_badges[:num_eagle] or []

    complete = bool(len(other_badges) == num_other and len(eagle_badges) == num_eagle)

    return eagle_badges, other_badges, complete


def get_life_merit_badges(
    badge_signoff_list, num_eagle=3, offset_eagle=4, num_other=2, offset_other=2
):
    """from a list of merit badge signoffs, return the 5th through 7th eagle-required
    merit badges, plus a list of the badges needed to get to 11 total earned"""

    """get the list of eagle required badges, sort by date"""

    if not badge_signoff_list:
        return [], [], False

    eagle_badges = (
        sorted(
            filter(lambda x: x.requirement.badge.eagle_required, badge_signoff_list),
            key=lambda x: (x.date, x.requirement.badge.name),
        )
        or []
    )

    """combine the list of non-eagle badges and the eagle badges beyond what are needed for this rank, sort the combined list, and assign the first num_other badges after the total offset"""
    non_eagle_badges = (
        sorted(
            filter(
                lambda x: not x.requirement.badge.eagle_required, badge_signoff_list
            ),
            key=lambda x: (x.date, x.requirement.badge.name),
        )
        or []
    )

    remaining_eagle_badges = eagle_badges[num_eagle + offset_eagle :]

    other_badges = sorted(
        chain(non_eagle_badges, remaining_eagle_badges),
        key=lambda x: (x.date, x.requirement.badge.name),
    )[offset_other : num_other + offset_other]

    """the eagle badges are the first num_eagle badges in the sorted eagle_badges list"""
    eagle_badges = eagle_badges[offset_eagle : num_eagle + offset_eagle]

    complete = bool(len(other_badges) == num_other and len(eagle_badges) == num_eagle)

    return eagle_badges, other_badges, complete


def get_eagle_merit_badges(badge_signoff_list):

    allocated_eagle_badges, rest = get_eagle_badges_for_eagle(badge_signoff_list)

    allocated_extra_badges, rest = get_remaining_badges_for_eagle(rest)

    allocated_eagle_badges.update(allocated_extra_badges)

    if len(allocated_eagle_badges) == 21:
        complete = True
    else:
        complete = False

    return (
        allocated_eagle_badges,
        sorted(rest, key=lambda x: (x.date, x.requirement.badge.name)),
        complete,
    )


def get_eagle_badges_for_eagle(badge_signoff_list):

    allocated_eagle_badges = {}
    remaining_badges = copy.copy(list(badge_signoff_list.all()))

    "get a list of {name of badge: code} either from the cache or create it"
    signed_eagle_badges = filter(
        lambda x: x.requirement.badge.eagle_required, badge_signoff_list
    )

    eagle_badge_dict = cache.get_or_set("eagle_badge_dict", setup_eagle_badge_dict())
    for signoff in signed_eagle_badges:
        if signoff.requirement.code not in allocated_eagle_badges:
            code = eagle_badge_dict[signoff.requirement.badge.name.strip()]
            allocated_eagle_badges[code] = signoff
            remaining_badges.remove(signoff)

    return allocated_eagle_badges, remaining_badges


def get_remaining_badges_for_eagle(badge_signoff_list):

    my_badge_signoff_list = copy.copy(
        sorted(badge_signoff_list, key=lambda x: (x.date, x.requirement.badge.name))
    )

    allocated_extra_badges = {}

    for code, signoff in zip("opqrstu", my_badge_signoff_list):
        allocated_extra_badges[f"03{code}"] = signoff
        badge_signoff_list.remove(signoff)

    return allocated_extra_badges, badge_signoff_list


def setup_eagle_badge_dict():
    eagle_merit_badge_requirements = (
        Requirement.objects.filter(
            badge__badge_type="rank", badge__name="Eagle", code__startswith="03"
        )
        .order_by("code")
        .all()
    )

    eagle_merit_badge_dict = defaultdict(str)

    for requirement in eagle_merit_badge_requirements:
        if requirement.code == "03":
            continue
        badges = requirement.text.strip().split(" OR ")
        for badge in badges:
            eagle_merit_badge_dict[badge.strip()] = requirement.code

    return eagle_merit_badge_dict
