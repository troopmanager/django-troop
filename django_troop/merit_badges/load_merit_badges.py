import logging
from pathlib import Path

import toml
from badges.models import Badge, get_mb_name
from django.conf import settings
from django.core.management.base import BaseCommand
from requirements.models import Requirement
from tqdm import tqdm

DATA_DIR = settings.DATA_DIR

logger = logging.getLogger(__name__)


def load_merit_badges(path, load_type):
    mb_data = toml.load(DATA_DIR / "merit_badges.toml")

    for merit_badge, data in tqdm(mb_data.items()):
        logger.info(merit_badge)
        badge, created = Badge.objects.get_or_create(
            name=get_mb_name(merit_badge),
            badge_type="merit_badge",
            eagle_required=data["eagle required"],
        )
        logger.info(badge)
        requirement, created = Requirement.objects.get_or_create(
            badge=badge, code="Completion", text=f"{badge.name} completion"
        )
        logger.info(requirement)
    ic("Load Merit Badges Complete")  # noqa


class Command(BaseCommand):
    help = "Loads Merit Badges"

    def add_arguments(self, parser):
        parser.add_argument("config_dir", nargs="?", default=DATA_DIR, type=str)

    def handle(self, *args, **kwargs):
        DATA_DIR = Path(kwargs["config_dir"])

        load_merit_badges(DATA_DIR, load_type=kwargs["load_type"])


def main(*args, **kwargs):
    Command().handle(config_dir=DATA_DIR, **kwargs)
