from django.db import models
from requirement_labels.models import RequirementLabel


class Requirement(models.Model):
    badge = models.ForeignKey(
        "badges.Badge", on_delete=models.CASCADE, related_name="requirements"
    )
    code = models.CharField(max_length=20)
    text = models.CharField(max_length=200, null=True)
    full_text = models.CharField(max_length=1000, null=True)
    equivalent_requirements = models.CharField(max_length=100, null=True)
    labels = models.ManyToManyField(RequirementLabel, related_name="requirements")
    quantity = models.IntegerField(null=True, blank=True)
    quantity_type = models.CharField(max_length=100, blank=True, null=True)
    # quantity types are "active_participation" (months), "leadership_position" (months)
    # "service_project" (hours), "camping nights" (nights), "leadership_position_no_bugler" (months)
    #

    def __str__(self):
        return f"{self.__class__.__name__} {self.badge.name} {self.code}"
