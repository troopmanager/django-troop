#!/bin/bash

set -exo pipefail

main () {
	python $APP_HOME/manage.py collectstatic --noinput
	python $APP_HOME/manage.py migrate
	python $APP_HOME/manage.py load_base
#	python $APP_HOME/manage.py loaddata $APP_HOME/data/common_files/initial_data.json
	gunicorn django_troop.wsgi --bind 0.0.0.0:8000 --reload --timeout 1200

}

main "$@"
