#!/usr/bin/env bash

set -exo pipefail

if [[ $# -ne 2 ]]; then
	echo "wrong number of arguments"
	echo "args: TROOP PORT"
	exit 1
fi

BASEDIR=${HOME}/projects/django-troop/django_troop
cd $BASEDIR

TROOP=$1
PORT=$2

if [[ ! -d $BASEDIR/nginx/conf.d ]]; then
	mkdir -p $BASEDIR/nginx/conf.d
fi

cat > $BASEDIR/nginx/conf.d/${TROOP}-nginx.conf << END_OF_NGINX
server {
	server_name ${TROOP}.troopmanager.org;
	location / {
		include proxy_params;
		proxy_pass http://127.0.0.1:${PORT};
	}
}

server {
    if (\$host = ${TROOP}.troopmanager.org) {
        return 301 https://\$host\$request_uri;
    } # managed by Certbot


	listen 80;
	server_name ${TROOP}.troopmanager.org;
    return 404; # managed by Certbot

}
END_OF_NGINX
