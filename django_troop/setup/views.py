from django.http import HttpResponse
from scouts.models import User

# Create your views here.


def setup(request):
    if User.objects.filter(is_superuser=True).exists():
        return HttpResponse("There is already a superuser")
    else:
        return HttpResponse("There is not a superuser")
