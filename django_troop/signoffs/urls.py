from django.urls import path

from . import views

urlpatterns = [
    path("update/", views.signoff_update, name="signoff_update"),
    path("delete/", views.signoff_delete, name="signoff_delete"),
    path("mini_update/", views.mini_signoff_update, name="mini_signoff_update"),
    path("mini_delete/", views.mini_signoff_delete, name="mini_signoff_delete"),
]
