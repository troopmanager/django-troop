from datetime import date as datetime_date

from django.db import models


class UnsignedRequirementsManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(signed=False)


class SignedRequirementsManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(signed=True)


class RankBadgeFinalSignoffsManager(models.Manager):
    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .filter(
                models.Q(requirement__final_requirement__isnull=False)
                | models.Q(requirement__text="Scout Completion"),
                requirement__badge__badge_type="rank",
            )
        )


class Signoff(models.Model):
    date = models.DateField(default=None, null=True, blank=True)
    quantity = models.FloatField(null=True, blank=True)
    scout = models.ForeignKey("scouts.User", on_delete=models.CASCADE)
    signed = models.BooleanField(default=False, blank=True)
    requirement = models.ForeignKey(
        "requirements.Requirement", on_delete=models.CASCADE, related_name="signoffs"
    )

    objects = models.Manager()
    unsigned_requirements = UnsignedRequirementsManager()
    signed_requirements = SignedRequirementsManager()
    final_signoffs = RankBadgeFinalSignoffsManager()

    class Meta:
        unique_together = ["requirement", "scout"]
        permissions = [
            ("signoff_allowed", "Can sign off requirements"),
            ("signoff_proposer", "Can propose signoffs to be approved"),
            ("signoff_approver", "Can approve proposed signoffs"),
            ("merit_badge_signer", "Can sign off merit badge requirements"),
        ]

    def __str__(self):
        return f"<{self.__class__.__name__} {self.scout.last_name} {self.requirement.badge.name } {self.requirement.code} {self.signed}>"

    def record_complete(self, date=None, signer=None):
        self.date = date or datetime_date.today()
        self.signed = True
        self.save()

    def reset_complete(self):
        self.date = None
        self.signed = False
        self.save()
