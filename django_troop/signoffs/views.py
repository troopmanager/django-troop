from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
from django.template import loader
from signoffs.models import Signoff


@permission_required("signoffs.signoff_allowed")
def signoff_update(request):
    template = loader.get_template("signoffs/signed_off.html")
    signoff = Signoff.objects.get(pk=request.POST.get("code"))
    signoff.record_complete()
    context = {"signoff": signoff}

    return HttpResponse(template.render(request=request, context=context))


@permission_required("signoffs.signoff_allowed")
def signoff_delete(request):
    template = loader.get_template("signoffs/not_signed_off.html")
    signoff = Signoff.objects.get(pk=request.POST.get("code"))
    signoff.reset_complete()
    context = {"signoff": signoff}

    return HttpResponse(template.render(request=request, context=context))


@permission_required("signoffs.signoff_allowed")
def mini_signoff_update(request):
    template = loader.get_template("signoffs/mini_signed_off.html")
    signoff = Signoff.objects.get(pk=request.POST.get("code"))
    signoff.record_complete()
    context = {"signoff": signoff}

    return HttpResponse(template.render(request=request, context=context))


@permission_required("signoffs.signoff_allowed")
def mini_signoff_delete(request):
    template = loader.get_template("signoffs/mini_not_signed_off.html")
    signoff = Signoff.objects.get(pk=request.POST.get("code"))
    signoff.reset_complete()
    context = {"signoff": signoff}

    return HttpResponse(template.render(request=request, context=context))
