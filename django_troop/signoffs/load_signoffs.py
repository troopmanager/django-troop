import csv
import logging
from datetime import datetime
from pathlib import Path

from badges.models import Badge, get_mb_name
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.management.base import BaseCommand
from icecream import ic
from requirements.models import Requirement
from scouts.models import User
from signoffs.models import Signoff
from tm_parser import Parser
from tqdm import tqdm

DATA_DIR = settings.DATA_DIR
REMOVE_PERIOD = str.maketrans("", "", ".")


RANK_EQUIVALENTS = {
    "2nd Bronze Palm": "Bronze Palm (2)",
    "2nd Gold Palm": "Gold Palm (2)",
    "2nd Silver Palm": "Silver Palm (2)",
}


logger = logging.getLogger(__name__)


def load_rank_signoffs(path, load_type):
    signoffs = [
        Signoff(requirement=requirement, scout=scout, signed=False, date=None)
        for scout in User.scouts.all()
        for requirement in Requirement.objects.filter(badge__badge_type="rank").all()
    ]

    Signoff.objects.bulk_create(
        signoffs,
        update_conflicts=True,
        update_fields=["date", "signed"],
        unique_fields=["requirement", "scout"],
    )

    match load_type:
        case "troopwebhost":
            with open(path / "rank_signoffs.csv", encoding="utf-8-sig") as f:
                number = len(f.readlines())
                f.seek(0)
                reader = csv.DictReader(f)
                for line in tqdm(reader, desc="Signoffs", total=number):
                    load_troopwebhost_signoff(line)

        case "troopmaster":
            p = Parser(path / "tm_data.pdf")
            for scout in tqdm(
                p.scouts.values(), desc="Scouts-signoffs", total=len(p.scouts.values())
            ):
                load_signoffs(scout)

    finish_up()
    record_whole_ranks()


def load_troopwebhost_signoff(line):
    if line["Date Earned"]:
        rank = Badge.objects.get(badge_type="rank", name=line["Rank"])
        name = User.name_from_string(line["Scout"])
        scout = User.scouts.get(
            last_name=name.get("last_name"),
            first_name=name.get("first_name"),
        )
        requirement = Requirement.objects.get(
            badge=rank, code=fix_code(line["Code"].strip())
        )

        signoff = Signoff.objects.get(scout=scout, requirement=requirement)

        signoff.date = datetime.strptime(line["Date Earned"], "%m/%d/%y").date()
        signoff.signed = True
        signoff.save()


def load_signoffs(scout, scout_obj):
    """given a scout set of data from the parser
    load all their rank-related signoffs"""
    if "Advancement" in scout:
        ranks = scout.get("Advancement").get("Ranks", None)
    else:
        ranks = scout.get("Ranks", None) or None
    if not ranks:
        logger.warning("no advancement found")
        return None
    for rank_str, data in ranks.items():
        if rank_str in RANK_EQUIVALENTS:
            rank_str = RANK_EQUIVALENTS[rank_str]
        logger.info(f"recording rank {rank_str}")
        if "Palm" in rank_str:
            continue
        rank, created = Badge.objects.get_or_create(badge_type="rank", name=rank_str)
        if created:
            logger.info(f"non-standard rank created: {rank_str}")
        if "Requirements" in data:
            for code, requirement_data in data.get("Requirements").items():
                if (
                    requirement_data
                    and rank.name in ("Star", "Life", "Eagle")
                    and code == "03"
                ):
                    for code, data in requirement_data.items():
                        if code == "Date":
                            record_signoff(
                                badge=rank, code="03", date=data, scout=scout_obj
                            )
                        elif data and "Date" in data:
                            record_signoff(
                                badge=rank,
                                code=f"03{code}",
                                date=data.get("Date"),
                                scout=scout_obj,
                            )
                else:
                    record_signoff(
                        badge=rank,
                        code=code,
                        date=requirement_data.get("Date"),
                        scout=scout_obj,
                    )
        logger.info(f"Completed {rank_str} for {scout_obj}")


def record_signoff(badge, code, date, scout):
    try:
        requirement, created = Requirement.objects.get_or_create(
            badge=badge, code=fix_code(code.strip())
        )
        if created:
            logger.info(f"new requirement created for {badge.name}, {code}")
            return
    except ObjectDoesNotExist:
        logger.warn(f"no requirement for badge: {badge} and code: {code}")
        return
    signoff, created = Signoff.objects.get_or_create(
        scout=scout, requirement=requirement
    )
    signoff.record_complete(date=date)
    logger.info(f"signoff {signoff} created for {scout}")
    signoff.save()


def fix_code(string):
    """takes format codes formatted like scoutbook has them and returns them normalized"""
    # 1.a -> 1a
    string = string.translate(REMOVE_PERIOD)

    if string.isdigit() and len(string) == 1:
        # 1, 4 -> 01, 04
        return "0" + string
    elif string.isdigit() and len(string) == 2:
        # 10, 11, 12 -> 10, 11, 12
        return string
    elif len(string) == 2:
        # 1a, 1c, 5a -> 01a, 01c, 05a
        return "0" + string
    elif len(string) == 3:
        # 10a, 11c -> 10a, 11c
        return string
    else:
        raise TypeError(f"poorly formatted rank requirement code: {string}")


def record_whole_ranks():
    for rank in Badge.objects.filter(badge_type="rank").all():
        for scout in User.scouts.all():
            try:
                signoff = Signoff.signed_requirements.get(
                    scout=scout, requirement=rank.final
                )
                scout.completed_ranks.add(signoff)
            except Signoff.DoesNotExist:
                continue


def record_complete_merit_badges(scout, scout_obj):

    if "Advancement" in scout:
        badges = scout.get("Advancement").get("Merit Badges", None)

    else:
        badges = scout.get("Merit Badges", None) or None

    ic(badges)
    if not badges:
        logger.warning("no merit badges found")
        return None

    for badge, data in badges.items():
        badge_obj, created = Badge.objects.get_or_create(
            name=get_mb_name(badge), badge_type="merit_badge"
        )
        if created:
            logger.warning(f"new badge {badge} had to be created")
            badge_obj.save()
        requirement, created = Requirement.objects.get_or_create(
            badge=badge_obj, code="Completion"
        )
        if created:
            logger.warning(f"new requirement {requirement} had to be created")
            requirement.save()
        s, created = Signoff.objects.get_or_create(
            date=data["Date"],
            scout=scout_obj,
            signed=True,
            requirement=requirement,
        )
        s.save()

        ic(s)

        logger.info(
            f"merit badge {badge} completed for {scout_obj.first_name} {scout_obj.last_name}"
        )


def start_a_badge(scout, badge):
    logger.info(f"starting a badge {badge} for scout {scout}")

    signoffs = []
    if Signoff.objects.filter(requirement__badge=badge, scout=scout).exists():
        logger.info("looks like this badge has already been started?")
        return None
    for requirement in Requirement.objects.filter(badge=badge).all():
        signoffs.append(
            Signoff(requirement=requirement, scout=scout, signed=False, date=None)
        )
        logger.info(f"adding requirement {requirement} for scout {scout}")
    Signoff.objects.bulk_create(signoffs)


def finish_up():
    for requirement in Requirement.objects.filter(badge__badge_type="rank").all():
        if requirement.equivalent_requirements:
            for scout in User.scouts.all():
                for signoff in Signoff.signed_requirements.filter(
                    scout=scout,
                    requirement__badge=requirement.badge,
                    requirement__code__in=requirement.equivalent_requirements.split(),
                ).all():
                    signoff.save()


class Command(BaseCommand):
    help = "Loads signoffs"

    def add_arguments(self, parser):
        parser.add_argument("config_dir", nargs="?", default=DATA_DIR, type=str)

    def handle(self, *args, **kwargs):
        DATA_DIR = Path(kwargs["config_dir"])

        load_rank_signoffs(DATA_DIR, load_type=kwargs["load_type"])


def main(*args, **kwargs):
    Command().handle(config_dir=DATA_DIR, **kwargs)
