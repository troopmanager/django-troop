#!/bin/bash

set -euxo pipefail


echo "running init tasks"
python $BASE_DIR/django_troop/manage.py collectstatic --noinput
python $BASE_DIR/django_troop/manage.py migrate
python $BASE_DIR/django_troop/manage.py load_base
#python $BASE_DIR/django_troop/manage.py loaddata $BASE_DIR/data/common_files/initial_data.json
python $BASE_DIR/django_troop/manage.py load_permissions
python $BASE_DIR/django_troop/manage.py ensure_superuser --no-input
