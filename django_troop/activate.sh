#!/bin/bash

set -euxo pipefail

./init.sh
./run-worker.sh &
./run.sh
