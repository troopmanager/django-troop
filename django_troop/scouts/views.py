import logging

from badges.models import Badge
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from scouts.models import User
from signoffs.models import Signoff

from .forms import AdultForm, ScoutForm, UserForm

logger = logging.getLogger(__name__)


class AdultListView(LoginRequiredMixin, ListView):
    context_object_name = "adults"
    template_name = "scouts/adults_list.html"
    ordering = ["last_name", "first_name"]
    queryset = User.adults.all()


class AdultDetailView(LoginRequiredMixin, DetailView):
    model = User
    context_object_name = "adult"
    template_name = "scouts/adult_detail.html"


class AdultUpdateView(PermissionRequiredMixin, UpdateView):
    model = User
    template_name = "scouts/form-example.html"
    form_class = AdultForm
    permission_required = [
        "scouts.change_user",
    ]

    def form_valid(self, form):
        form.instance.user_type = "adult"
        return super().form_valid(form)

    def get_form_kwargs(self):
        return super().get_form_kwargs()


class AdultCreateView(PermissionRequiredMixin, CreateView):
    model = User
    template_name = "scouts/form-example.html"
    form_class = AdultForm
    permission_required = [
        "scouts.add_user",
    ]

    def form_valid(self, form):
        form.instance.user_type = "adult"
        return super().form_valid(form)


class AdultDeleteView(PermissionRequiredMixin, DeleteView):
    model = User
    success_url = reverse_lazy("adult_list")
    permission_required = ["scouts.delete_user"]


class ScoutListView(LoginRequiredMixin, ListView):
    context_object_name = "scouts"
    template_name = "scouts/scouts_list.html"
    ordering = ["last_name", "first_name"]
    queryset = User.scouts

    def get_context_data(self, **kwargs):
        logger.warning("in get_context_data")
        context = super().get_context_data(**kwargs)
        for scout in context["scouts"]:
            if (
                scout.current_rank and scout.current_rank.order >= 1
            ):  # scout rank or higher
                scout.current_rank_signoff = Signoff.objects.get(
                    scout=scout,
                    requirement=scout.current_rank.final,
                )

        return context


class ScoutDetailView(LoginRequiredMixin, DetailView):
    model = User
    context_object_name = "scout"
    template_name = "scouts/scouts_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if context["scout"].current_rank is None:
            context["signoffs"] = (
                Signoff.objects.filter(
                    requirement__badge__badge_type="rank", scout=context["scout"]
                )
                .exclude(
                    requirement__badge__gt=Badge.objects.get(
                        badge_type="rank", name="First Class"
                    )
                )
                .order_by("requirement__badge__order", "requirement__code")
                .all()
            )

        elif (
            context["scout"].current_rank.order
            < Badge.objects.get(badge_type="rank", name="First Class").order
        ):
            context["signoffs"] = (
                Signoff.objects.filter(
                    requirement__badge__badge_type="rank", scout=context["scout"]
                )
                .exclude(
                    requirement__badge__lte=context["scout"].current_rank,
                )
                .exclude(
                    requirement__badge__gt=Badge.objects.get(
                        badge_type="rank", name="First Class"
                    )
                )
                .order_by("requirement__badge__order", "requirement__code")
                .all()
            )
        else:
            context["signoffs"] = (
                Signoff.objects.filter(
                    requirement__badge__badge_type="rank", scout=context["scout"]
                )
                .filter(
                    requirement__badge__order=context["scout"].current_rank.order + 1
                )
                .order_by("requirement__badge__order", "requirement__code")
                .all()
            )

        return context


class ScoutUpdateView(PermissionRequiredMixin, UpdateView):
    model = User
    template_name = "scouts/form-example.html"
    form_class = ScoutForm
    permission_required = [
        "scouts.change_user",
    ]

    queryset = User.scouts

    def form_valid(self, form):
        form.instance.user_type = "scout"
        return super().form_valid(form)


class ScoutCreateView(PermissionRequiredMixin, CreateView):
    model = User
    template_name = "scouts/form-example.html"
    form_class = ScoutForm
    permission_required = [
        "scouts.add_user",
    ]

    def form_valid(self, form):
        form.instance.save()
        form.instance.user_type = "scout"
        form.instance.current_rank = Badge.objects.get(
            badge_type="rank", order=0
        )  # "No Rank"
        form.instance.initial_signoffs()
        return super().form_valid(form)


class UserUpdateView(PermissionRequiredMixin, UpdateView):
    model = User
    template_name = "scouts/form-example.html"
    form_class = UserForm
    permission_required = [
        "scouts.change_user",
    ]

    def form_valid(self, form):
        return super().form_valid(form)
