import logging
import re

from badges.models import Badge
from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models
from django.urls import reverse_lazy
from merit_badges.utils import get_life_merit_badges
from requirements.models import Requirement
from signoffs.models import Signoff

logger = logging.getLogger(__name__)


class ScoutManager(UserManager):
    def get_queryset(self):
        return super().get_queryset().filter(user_type="scout")


class AdultManager(UserManager):
    def get_queryset(self):
        return super().get_queryset().filter(user_type="adult")


class User(
    AbstractUser,
):
    user_type = models.CharField(
        max_length=10,
        default="other",
        choices=(("scout", "scout"), ("adult", "adult"), ("other", "other")),
    )
    first_name = models.CharField(max_length=150, blank=True, null=True)
    last_name = models.CharField(max_length=150)
    middle_name = models.CharField(max_length=150, blank=True, null=True)
    nick_name = models.CharField(max_length=150, blank=True, null=True)
    suffix = models.CharField(max_length=150, blank=True, null=True)

    pronouns = models.CharField(max_length=150, blank=True, null=True)

    age = models.IntegerField(null=True, blank=True)
    patrol = models.ForeignKey(
        "patrols.Patrol",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="patrol_members",
    )
    current_rank = models.ForeignKey(
        "badges.Badge",
        on_delete=models.SET_NULL,
        null=True,
        related_name="current_rank_scouts",
    )
    parents = models.ManyToManyField("self", blank=True)
    children = models.ManyToManyField("self", blank=True)

    completed_ranks = models.ManyToManyField(
        "signoffs.Signoff",
        related_name="completed_ranks_scouts",
        blank=True,
    )

    completed_merit_badges = models.ManyToManyField(
        "signoffs.Signoff",
        related_name="completed_merit_badges_scouts",
        blank=True,
    )

    class Meta:
        permissions = [
            (
                "import_users",
                "Can import users from source files",
            ),
            (
                "edit_user_permissions",
                "Can edit user permissions",
            ),
            (
                "invite_users",
                "Can invite new users",
            ),
        ]

    objects = UserManager()
    scouts = ScoutManager()
    adults = AdultManager()

    def get_absolute_url(self):
        if self.user_type == "scout":
            return reverse_lazy("scouts_detail", kwargs={"pk": self.pk})
        elif self.user_type == "adult":
            return reverse_lazy("adult_detail", kwargs={"pk": self.pk})

    @classmethod
    def name_from_string(cls, text):
        # Matches "Perkins, Michael "Mike""
        pat1 = re.compile(r'^(.*), (.*?)( "(.*)")?$')

        # Matches "Michael Perkins"
        pat2 = re.compile(r"^(.*) (.*)$")

        if m := pat1.match(text.strip()):
            last_name = m.group(1).strip()
            first_name = m.group(2).strip()
            nick_name = m.group(4).strip() if m.group(4) else None

        elif m := pat2.match(text.strip()):
            last_name = m.group(2).strip()
            first_name = m.group(1).strip()
            nick_name = None

        else:
            last_name = text

        return {
            "last_name": last_name,
            "first_name": first_name,
            "nick_name": nick_name,
        }

    @property
    def current_rank_date(self):
        if not self.current_rank:
            return None

        return Signoff.objects.get(scout=self, requirement=self.current_rank.final).date

    @property
    def next_rank(self):
        if not self.current_rank:
            return Badge.objects.get(badge_type="rank", name="Scout")
        return Badge.objects.get(badge_type="rank", order=self.current_rank.order + 1)

    @property
    def percent_rank_done(self):
        try:
            return (
                Signoff.objects.filter(
                    scout=self, requirement__badge=self.next_rank, signed=True
                ).count()
                / Signoff.objects.filter(
                    scout=self, requirement__badge=self.next_rank
                ).count()
            ) * 100.0
        except ZeroDivisionError:
            return 0

    @property
    def full_name(self):
        if self.nick_name:
            return f'{self.first_name} "{self.nick_name}" {self.last_name}'
        return f"{self.first_name} {self.last_name}"

    @property
    def safe_name(self):
        return f"{self.first_name} {self.last_name[0]}"

    def is_badge_complete(self, badge):
        return badge.is_complete(self)

    def start_badge(self, badge):
        badge.start_user(self)

    def start_new_scout_ranks(self):
        self.current_rank = Badge.objects.get(badge_type="rank", name="No Rank")
        self.save()
        for rank_order in range(0, 5):  # provide signoffs through first class (4)
            badge = Badge.objects.get(order=rank_order)
            self.start_badge(badge=badge)

    def start_next_rank(self):
        self.start_badge(self.next_rank)

    def signoff_completed_badge(self, badge):
        badge.finish_user(self)

    def get_star_merit_badges(self, num_eagle=4, num_total=6):
        # get a list of the first six earned merit badges for the scout, ordered by date (increasing)
        earned_merit_badges = list(
            Signoff.objects.filter(
                reqiurement__badge__type="Merit Badge",
                signed=True,
                scout=self,
                requirement__code="Completion",
            )
            .order_by("date")
            .limit(num_total)
        )

        # get a list of the first four eagle merit badges

        eagle_badges = list(
            Signoff.objects.filter(
                requirement__badge__type="Merit Badge",
                signed=True,
                scout=self,
                requirement__code="Completion",
                requirement__badge__eagle_required=True,
            )
            .order_by("date")
            .limit(num_eagle)
        )

        # the extra badges are the first ones earned that weren't used for the earned_eagle badges
        extra_badges = [
            badge for badge in earned_merit_badges if badge not in eagle_badges
        ][: num_total - num_eagle]
        return eagle_badges, extra_badges

    def allocate_life_merit_badges(
        self,
    ):

        """get list of eagle badges and other badges that should count for life rank"""
        eagle_badges, other_badges, all_complete = get_life_merit_badges(
            badge_signoff_list=Signoff.objects.filter(
                scout=self,
                requirement__badge__badge_type="Merit Badge",
                signed=True,
                requirement__code="Completion",
            ).all()
        )

    def get_eagle_merit_badges(self):
        earned_non_eagle_merit_badges = list(
            Signoff.objects.filter(
                reqiurement__badge__badge_type="Merit Badge",
                signed=True,
                scout=self,
                requirement__code="Completion",
                requirement__badge__eagle_required=False,
            )
            .order_by("date")
            .limit(7)
        )

        eagle_badges = list(
            Signoff.objects.filter(
                requirement__badge__type="Merit Badge",
                signed=True,
                scout=self,
                requirement__code="Completion",
                requirement__badge__eagle_required=True,
            )
            .order_by("date")
            .all()
        )

        earned_eagle_badges = {}
        extra_badges = list()

        for badge in eagle_badges:
            requirement = Requirement.objects.filter(
                text__icontains=badge.requirement.badge.name
            )

            if requirement.code not in earned_eagle_badges:
                earned_eagle_badges.set(requirement.code, badge)
            else:
                extra_badges.append(badge)

        extra_badges.extend(earned_non_eagle_merit_badges)

        earned_extra_badges = sorted(extra_badges, key=lambda x: x.date)[:7]

        return earned_eagle_badges, earned_extra_badges

    @classmethod
    def get_scout_from_data(cls, data):
        logger.info(data["Data"].get("Name", None))
        if "Data" in data and data["Data"].get("Name", None):
            scout_name = User.name_from_string(data["Data"].get("Name"))
            first_name = scout_name.get("first_name")
            last_name = scout_name.get("last_name")
            nick_name = scout_name.get("nick_name")
        else:
            first_name = data["Data"].get("First Name")
            last_name = data["Data"].get("Last Name")
            nick_name = data["Data"].get("Nick Name")
        username = f"{first_name}{last_name}"
        scout_obj, created = cls.scouts.get_or_create(
            username=username, last_name=last_name, first_name=first_name
        )
        scout_obj.nick_name = nick_name
        scout_obj.user_type = "scout"
        scout_obj.save()
        return scout_obj, created
