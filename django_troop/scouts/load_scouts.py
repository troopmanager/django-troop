import logging
from pathlib import Path

from badges.models import Badge
from celery import shared_task
from django.conf import settings
from django.core.management.base import BaseCommand
from patrols.models import Patrol
from scouts.models import User
from signoffs.load_signoffs import (
    load_signoffs,
    record_complete_merit_badges,
    start_a_badge,
)
from signoffs.models import Signoff

RANK_ORDER = {
    "No Rank": 0,
    "Scout": 1,
    "Tenderfoot": 2,
    "Second Class": 3,
    "First Class": 4,
    "Star": 5,
    "Life": 6,
    "Eagle": 7,
}

DATA_DIR = settings.DATA_DIR

logger = logging.getLogger(__name__)

logging.basicConfig(level=logging.DEBUG)


RANK_EQUIVALENTS = {
    "2nd Bronze Palm": "Bronze Palm (2)",
    "2nd Gold Palm": "Gold Palm (2)",
    "2nd Silver Palm": "Silver Palm (2)",
}


def load_scouts(path, load_type):
    pass


@shared_task
def load_scout(data):
    scout_obj, created = User.get_scout_from_data(data)
    logger.warning(f"created scout object {scout_obj.full_name}")

    load_signoffs(data, scout_obj)
    logger.warning(f"logged signoffs for {scout_obj.full_name}")

    scout_obj.start_new_scout_ranks()  # thorugh first class
    logger.warning(f"created initial signoffs for {scout_obj.full_name}")

    while scout_obj.next_rank.is_complete(scout_obj):
        logger.info(f"finishing rank {scout_obj.next_rank.name}")
        scout_obj.next_rank.finish_user(scout_obj)
    scout_obj.save()
    logger.info(f"logged signed off ranks {scout_obj.full_name}")

    logger.warning(f"my rank is {scout_obj.current_rank.name}")

    save_current_rank(data, scout_obj)
    logger.info(
        f"added current_rank {scout_obj.current_rank.name} to {scout_obj.full_name}"
    )

    save_patrol(data, scout_obj)
    logger.info(f"added patrol {scout_obj.patrol.name} to {scout_obj.full_name}")

    save_age(data, scout_obj)
    logger.info(f"added age {scout_obj.age} to {scout_obj.full_name}")

    record_complete_merit_badges(data, scout_obj)
    logger.info(f"signed off merit badges for {scout_obj.full_name}")

    logger.info("Saving Scout . . . ")
    scout_obj.save()

    logger.warning(
        f"COMPLETE: added {scout_obj}, member of {scout_obj.patrol.name}, age {scout_obj.age}, rank {scout_obj.current_rank.name}"
    )  # noqa


def save_current_rank(scout, scout_obj):
    if "Data" in scout and scout["Data"].get("Rank"):
        current_rank = scout["Data"].get("Rank").strip()
    elif "Rank" in scout:
        if scout.get("Rank") in RANK_EQUIVALENTS:
            current_rank = RANK_EQUIVALENTS[scout.get("Rank").strip()]
        else:
            current_rank = scout.get("Rank").strip()
    else:
        current_rank = "No Rank"
    current_rank = Badge.objects.get(badge_type="rank", name=current_rank)
    logger.warning(
        f"current rank from file: {current_rank} from calculation:{scout_obj.current_rank}"
    )
    assert scout_obj.current_rank == current_rank
    scout_obj.current_rank = current_rank
    scout_obj.save()


def start_next_ranks(scout_obj):
    # start the scout off on their next rank. If the next rank is below first class,
    # start them on all ranks through first class.
    next_rank_index = scout_obj.current_rank.order + 1

    for i in range(
        next_rank_index, 5
    ):  # take care of ranks scout through first class (4)
        next_rank = Badge.objects.get(badge_type="rank", order=i)
        start_a_badge(scout_obj, next_rank)
    if 4 < next_rank_index <= 7:  # 7 is Eagle - this gets ranks star through Eagle
        next_rank = Badge.objects.get(badge_type="rank", order=next_rank_index)
        start_a_badge(scout_obj, next_rank)


def save_patrol(scout, scout_obj):
    patrol = scout.get("Patrol") or scout["Data"].get("Patrol")
    if patrol:
        patrol, created = Patrol.objects.get_or_create(name=patrol.strip())
        if created:
            patrol.save()
    scout_obj.patrol = patrol


def save_age(data, scout_obj):
    try:
        age = int(data["Data"].get("Age", "1"))
    except (TypeError, ValueError) as e:
        age = 1
        logger.warning(f"invalid scout age: {e}")
    scout_obj.age = age


def record_current_ranks(data, scout_obj):

    # obtain the signoffs for this scout for all ranks that have been completed
    rank_signoffs = Signoff.final_signoffs.filter(scout=scout_obj, signed=True).all()

    for signoff in rank_signoffs:
        # loop through and add these to the completed ranks for that scout
        scout_obj.completed_ranks.add(signoff)

        # and make sure all that badge's signoffs are at least signed=True
        logger.warning(
            f"signing off all requirements for {signoff.requirement.badge.name}"
        )  # noqa
        scout_obj.signoff_completed_badge(signoff.requirement.badge)

    if rank_signoffs:
        # get the current rank they've completed (it's the one with the highest index)
        logger.warning(f"finding highest rank signoff from {rank_signoffs}")  # noqa
        current_rank_index = max(
            (signoff.requirement.badge.order for signoff in rank_signoffs)
        )
        logger.warning(f"highest rank signoff was {current_rank_index}")  # noqa

        scout_obj.current_rank = Badge.objects.get(
            order=current_rank_index, badge_type="rank"
        )
    else:
        logger.warning("scout has no rank, setting No Rank")  # noqa
        scout_obj.current_rank = Badge.objects.get(order=0, badge_type="rank")
        current_rank_index = 0

    scout_obj.save()


class Command(BaseCommand):
    help = "Loads scouts, patrols"

    def add_arguments(self, parser):
        parser.add_argument("config_dir", nargs="?", default=DATA_DIR, type=str)

    def handle(self, *args, **kwargs):
        DATA_DIR = Path(kwargs["config_dir"])

        load_scouts(DATA_DIR, kwargs["load_type"])


def main(*args, **kwargs):
    Command().handle(config_dir=DATA_DIR, **kwargs)
