import django_tables2 as tables
from scouts.models import User


class ScoutTable(tables.Table):
    class Meta:
        model = User
        template_name = "scouts/bootstrap_htmx_table.html"
