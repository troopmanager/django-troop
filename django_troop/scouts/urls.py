from django.urls import path

from . import views

urlpatterns = [
    path("", views.ScoutListView.as_view(), name="scouts_list"),
    path("<int:pk>", views.ScoutDetailView.as_view(), name="scouts_detail"),
    path("edit/<int:pk>/", views.ScoutUpdateView.as_view(), name="scouts_edit"),
    path("edit/new/", views.ScoutCreateView.as_view(), name="scouts_create"),
]
