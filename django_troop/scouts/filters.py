import django_filters
from django.db.models import Q
from scouts.models import User


class ScoutFilter(django_filters.FilterSet):
    query = django_filters.CharFilter(method="universal_search", label="")

    class Meta:
        model = User
        fields = ["query"]

    def universal_search(self, queryset, name, value):
        return User.scouts.filter(
            Q(first_name__icontains=value)
            | Q(last_name__icontains=value)
            | Q(patrol__name__icontains=value)
        )
