import datetime
import logging
import sys
from pprint import pprint

import yaml
from badges.models import Badge
from django.core.management import call_command
from django.test import TestCase
from patrols.models import Patrol
from scouts.load_scouts import load_scout
from scouts.models import User
from signoffs.models import Signoff

logger = logging.getLogger(__name__)

stream_handler = logging.StreamHandler(stream=sys.stdout)
logger.addHandler(stream_handler)

# Create your tests here.


class EarlyMiddleScoutImportTests(TestCase):
    def setUp(self):
        call_command("load_base")
        with open("data/example_scouts/early_middle_scout.yaml") as f:
            self.middle_scout = yaml.safe_load(f)

        for name, value in self.middle_scout.items():
            pprint(value)
            load_scout(value)

        self.scout = User.objects.get(last_name="Larsen", first_name="Reggie")
        self.patrol = Patrol.objects.get(name="Big Kahunas")
        self.rank = Badge.objects.get(name="Tenderfoot")

    def test_scout_exist(self):

        signoffs = Signoff.objects.filter(scout=self.scout).all()
        for item in signoffs:
            pprint(item)

        self.assertEqual(self.scout.age, 15)
        self.assertEqual(self.scout.patrol, self.patrol)
        self.assertEqual(self.scout.current_rank, self.rank)

    def test_scout_signoffs(self):

        test_signoff = Signoff.objects.get(
            scout=self.scout, requirement__code="01a", requirement__badge__name="Scout"
        )

        self.assertEqual(test_signoff.date, datetime.date(2020, 10, 15))
        self.assertEqual(test_signoff.signed, True)

    def test_tenderfoot_signoffs(self):

        test_signoff = Signoff.objects.get(
            scout=self.scout,
            requirement__code="10",
            requirement__badge__name="Tenderfoot",
        )

        self.assertEqual(test_signoff.date, datetime.date(2022, 12, 15))
        self.assertEqual(test_signoff.signed, True)

    def test_signoff_due_to_completed_rank(self):
        test_signoff = Signoff.objects.get(
            scout=self.scout,
            requirement__code="07b",
            requirement__badge__name="Tenderfoot",
        )

        self.assertEqual(test_signoff.date, None)
        self.assertEqual(test_signoff.signed, True)

    def test_signoff_assigned_future_rank(self):
        test_signoff = Signoff.objects.get(
            scout=self.scout,
            requirement__code="02a",
            requirement__badge__name="Second Class",
        )

        self.assertEqual(test_signoff.date, None)
        self.assertEqual(test_signoff.signed, False)

    def test_merit_badge(self):

        test_signoff = Signoff.objects.get(
            scout=self.scout,
            requirement__code="Completion",
            requirement__badge__name="Sculpture",
        )

        self.assertEqual(test_signoff.date, datetime.date(2023, 6, 29))
        self.assertEqual(test_signoff.signed, True)

    def test_eagle_merit_badge(self):
        test_signoff = Signoff.objects.get(
            scout=self.scout, requirement__code="03p", requirement__badge__name="Eagle"
        )

        self.assertEqual(test_signoff.date, datetime.date(2023, 6, 29))
        self.assertEqual(test_signoff.signed, True)

    def test_star_merit_badge(self):
        test_signoff = Signoff.objects.get(
            scout=self.scout, requirement__code="03e", requirement__badge__name="Star"
        )

        self.assertEqual(test_signoff.date, datetime.date(2023, 6, 29))
        self.assertEqual(test_signoff.signed, True)

    def test_life_merit_badge(self):
        test_signoff = Signoff.objects.get(
            scout=self.scout, requirement__code="03d", requirement__badge__name="Life"
        )

        self.assertEqual(test_signoff.date, datetime.date(2023, 6, 29))
        self.assertEqual(test_signoff.signed, True)
