import datetime
import logging
import sys
from pprint import pprint

import yaml
from badges.models import Badge
from django.core.management import call_command
from django.test import TestCase
from patrols.models import Patrol
from scouts.load_scouts import load_scout
from scouts.models import User
from signoffs.models import Signoff

logger = logging.getLogger(__name__)

stream_handler = logging.StreamHandler(stream=sys.stdout)
logger.addHandler(stream_handler)

# Create your tests here.


class EarlyScoutImportTests(TestCase):
    def setUp(self):
        call_command("load_base")
        with open("data/example_scouts/early_scout.yaml") as f:
            self.scout = yaml.safe_load(f)

        for name, value in self.scout.items():
            pprint(value)
            load_scout(value)

        self.scout = User.objects.get(last_name="Myrna", first_name="Lawrence")
        self.patrol = Patrol.objects.get(name="The Roman Legion")
        self.rank = Badge.objects.get(name="Scout")

    def test_scout_exits(self):

        signoffs = Signoff.objects.filter(scout=self.scout).all()
        for item in signoffs:
            pprint(item)

        self.assertEqual(self.scout.age, 14)
        self.assertEqual(self.scout.patrol, self.patrol)
        self.assertEqual(self.scout.current_rank, self.rank)

    def test_scout_signoffs(self):

        test_signoff = Signoff.objects.filter(
            scout=self.scout, requirement__code="01a", requirement__badge__name="Scout"
        ).all()

        self.assertEqual(test_signoff[0].date, datetime.date(2023, 10, 22))
        self.assertEqual(test_signoff[0].signed, True)

    def test_scout_pending_signoffs(self):

        test_not_signed_off = Signoff.objects.filter(
            scout=self.scout,
            requirement__code="07a",
            requirement__badge__name="First Class",
        ).all()

        self.assertEqual(test_not_signed_off[0].date, None)
        self.assertEqual(test_not_signed_off[0].signed, False)
