import datetime
import logging
import sys

import yaml
from badges.models import Badge
from django.core.management import call_command
from django.test import TestCase
from patrols.models import Patrol
from scouts.load_scouts import load_scout
from scouts.models import User
from signoffs.models import Signoff

logger = logging.getLogger(__name__)

stream_handler = logging.StreamHandler(stream=sys.stdout)
logger.addHandler(stream_handler)

# Create your tests here.


class OlderScoutImportTests(TestCase):
    def setUp(self):
        call_command("load_base")
        with open("data/example_scouts/older_scout.yaml") as f:
            self.older_scout = yaml.safe_load(f)

        for name, value in self.older_scout.items():
            load_scout(value)

        self.scout = User.objects.get(last_name="Santo", first_name="Conrad")
        self.patrol = Patrol.objects.get(name="The Roman Empire")
        self.rank = Badge.objects.get(name="Eagle")

    def test_scout_exist(self):

        self.assertEqual(self.scout.first_name, "Conrad")
        self.assertEqual(self.scout.last_name, "Santo")

    def test_second_class_signoffs(self):
        test_signoff = Signoff.objects.get(
            scout=self.scout,
            requirement__code="12",
            requirement__badge__name="Second Class",
        )

        self.assertEqual(test_signoff.date, datetime.date(2020, 2, 13))
        self.assertEqual(test_signoff.signed, True)

    def test_tenderfoot_signoffs(self):
        test_signoff = Signoff.objects.get(
            scout=self.scout,
            requirement__code="11",
            requirement__badge__name="Tenderfoot",
        )

        self.assertEqual(test_signoff.date, datetime.date(2019, 8, 15))
        self.assertEqual(test_signoff.signed, True)

    def test_current_rank(self):
        self.assertEqual(self.scout.current_rank, self.rank)

    def test_patrol(self):
        self.assertEqual(self.scout.patrol, self.patrol)

    def test_age(self):
        self.assertEqual(self.scout.age, 16)

    def test_non_eagle_merit_badge(self):
        test_signoff = Signoff.objects.get(
            scout=self.scout,
            requirement__code="Completion",
            requirement__badge__name="Astronomy",
        )

        self.assertEqual(test_signoff.date, datetime.date(2022, 10, 8))
        self.assertEqual(test_signoff.signed, True)

    def test_eagle_merit_badge(self):
        test_signoff = Signoff.objects.get(
            scout=self.scout,
            requirement__code="Completion",
            requirement__badge__name="Cooking",
        )

        self.assertEqual(test_signoff.date, datetime.date(2022, 7, 1))
        self.assertEqual(test_signoff.signed, True)

    def test_allocation_star_eagle_merit_badge(self):
        test_signoff = Signoff.objects.get(
            scout=self.scout, requirement__code="03d", requirement__badge__name="Star"
        )

        self.assertEqual(test_signoff.date, datetime.date(2021, 4, 7))
        self.assertEqual(test_signoff.signed, True)

    def test_allocation_star_non_eagle_merit_badge(self):
        test_signoff = Signoff.objects.get(
            scout=self.scout, requirement__code="03f", requirement__badge__name="Star"
        )

        self.assertEqual(test_signoff.date, datetime.date(2019, 6, 25))
        self.assertEqual(test_signoff.signed, True)

    def test_allocation_life_non_eagle_merit_badge(self):
        test_signoff = Signoff.objects.get(
            scout=self.scout, requirement__code="03d", requirement__badge__name="Life"
        )

        self.assertEqual(test_signoff.date, datetime.date(2019, 9, 26))
        self.assertEqual(test_signoff.signed, True)

    def test_allocation_life_eagle_merit_badge(self):
        test_signoff = Signoff.objects.get(
            scout=self.scout, requirement__code="03c", requirement__badge__name="Life"
        )

        self.assertEqual(test_signoff.date, datetime.date(2021, 7, 2))
        self.assertEqual(test_signoff.signed, True)

    def test_allocation_first_aid_merit_badge_for_eagle(self):
        test_signoff = Signoff.objects.get(
            scout=self.scout, requirement__code="03a", requirement__badge__name="Eagle"
        )

        self.assertEqual(test_signoff.date, datetime.date(2021, 1, 1))
        self.assertEqual(test_signoff.signed, True)

    def test_allocation_personal_management_merit_badge(self):
        test_signoff = Signoff.objects.get(
            scout=self.scout, requirement__code="03k", requirement__badge__name="Eagle"
        )

        self.assertEqual(test_signoff.date, datetime.date(2021, 4, 7))
        self.assertEqual(test_signoff.signed, True)

    def test_allocation_elective_merit_badge(self):
        test_signoff = Signoff.objects.get(
            scout=self.scout, requirement__code="03u", requirement__badge__name="Eagle"
        )

        self.assertEqual(test_signoff.date, datetime.date(2020, 7, 13))
        self.assertEqual(test_signoff.signed, True)
