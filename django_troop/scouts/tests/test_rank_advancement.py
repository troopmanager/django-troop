import logging
import sys

import yaml
from badges.models import Badge
from django.core.management import call_command
from django.test import TestCase
from patrols.models import Patrol
from scouts.load_scouts import load_scout
from scouts.models import User
from signoffs.models import Signoff

logger = logging.getLogger(__name__)

stream_handler = logging.StreamHandler(stream=sys.stdout)
logger.addHandler(stream_handler)

# Create your tests here.


class RankAdvancementTest(TestCase):
    def setUp(self):
        call_command("load_base")
        with open("data/example_scouts/early_scout.yaml") as f:
            self.scout = yaml.safe_load(f)

        for name, value in self.scout.items():
            load_scout(value)

        self.scout = User.objects.get(last_name="Myrna", first_name="Lawrence")
        self.patrol = Patrol.objects.get(name="The Roman Legion")
        self.rank = Badge.objects.get(name="Scout")

        self.next_rank = Badge.objects.get(name="Tenderfoot")

    def test_earn(self):
        self.next_rank.finish_user(self.scout)
        self.assertEqual(self.scout.current_rank.name, "Tenderfoot")

    def test_not_signed_off(self):
        signoff = Signoff.objects.get(
            scout=self.scout,
            requirement__code="01a",
            requirement__badge__name="Tenderfoot",
        )
        self.assertEqual(signoff.date, None)
        self.assertEqual(signoff.signed, False)

    def test_signoffs(self):
        print("Finishing rank: ", end="")
        print(self.scout.next_rank.order, self.scout.next_rank.name)
        self.next_rank.finish_user(self.scout)
        signoff = Signoff.objects.get(
            scout=self.scout,
            requirement__code="01a",
            requirement__badge__name="Tenderfoot",
        )
        self.assertEqual(signoff.date, None)
        self.assertEqual(signoff.signed, True)
