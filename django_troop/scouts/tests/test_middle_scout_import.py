import datetime
import logging
import sys

import yaml
from badges.models import Badge
from django.core.management import call_command
from django.test import TestCase
from patrols.models import Patrol
from scouts.load_scouts import load_scout
from scouts.models import User
from signoffs.models import Signoff

logger = logging.getLogger(__name__)

stream_handler = logging.StreamHandler(stream=sys.stdout)
logger.addHandler(stream_handler)

# Create your tests here.


class MiddleScoutImportTests(TestCase):
    def setUp(self):
        call_command("load_base")
        with open("data/example_scouts/middle_scout.yaml") as f:
            self.middle_scout = yaml.safe_load(f)

        for name, value in self.middle_scout.items():
            load_scout(value)

        self.scout = User.objects.get(last_name="Ray", first_name="Hollie")
        self.patrol = Patrol.objects.get(name="McMen")
        self.rank = Badge.objects.get(name="First Class")

    def test_scout_exist(self):

        self.assertEqual(self.scout.first_name, "Hollie")
        self.assertEqual(self.scout.last_name, "Ray")

    def test_scout_signoffs(self):
        test_signoff = Signoff.objects.get(
            scout=self.scout, requirement__code="01a", requirement__badge__name="Scout"
        )

        self.assertEqual(test_signoff.date, datetime.date(2022, 6, 14))
        self.assertEqual(test_signoff.signed, True)

    def test_tenderfoot_signoffs(self):
        test_signoff = Signoff.objects.get(
            scout=self.scout,
            requirement__code="10",
            requirement__badge__name="Tenderfoot",
        )

        self.assertEqual(test_signoff.date, datetime.date(2022, 8, 4))
        self.assertEqual(test_signoff.signed, True)

    def test_first_class_signoffs(self):
        test_signoff = Signoff.objects.get(
            scout=self.scout,
            requirement__code="03c",
            requirement__badge__name="First Class",
        )

        self.assertEqual(test_signoff.date, None)
        self.assertEqual(test_signoff.signed, True)

    def test_star_signoff_assigned(self):
        test_signoff = Signoff.objects.get(
            scout=self.scout,
            requirement__code="02",
            requirement__badge__name="Star",
        )

        self.assertEqual(test_signoff.date, None)
        self.assertEqual(test_signoff.signed, False)

    def test_current_rank(self):
        self.assertEqual(self.scout.current_rank, self.rank)

    def test_patrol(self):
        self.assertEqual(self.scout.patrol, self.patrol)

    def test_age(self):
        self.assertEqual(self.scout.age, 13)

    def test_non_eagle_merit_badge(self):
        test_signoff = Signoff.objects.get(
            scout=self.scout,
            requirement__code="Completion",
            requirement__badge__name="Canoeing",
        )

        self.assertEqual(test_signoff.date, datetime.date(2022, 7, 1))
        self.assertEqual(test_signoff.signed, True)

    def test_eagle_merit_badge(self):
        test_signoff = Signoff.objects.get(
            scout=self.scout,
            requirement__code="Completion",
            requirement__badge__name="First Aid",
        )

        self.assertEqual(test_signoff.date, datetime.date(2023, 6, 30))
        self.assertEqual(test_signoff.signed, True)

    def test_allocation_star_eagle_merit_badge(self):
        test_signoff = Signoff.objects.get(
            scout=self.scout, requirement__code="03a", requirement__badge__name="Star"
        )

        self.assertEqual(test_signoff.date, datetime.date(2022, 7, 1))
        self.assertEqual(test_signoff.signed, True)

    def test_allocation_star_non_eagle_merit_badge(self):
        test_signoff = Signoff.objects.get(
            scout=self.scout, requirement__code="03e", requirement__badge__name="Star"
        )

        self.assertEqual(test_signoff.date, datetime.date(2022, 7, 1))
        self.assertEqual(test_signoff.signed, True)

    def test_allocation_life_non_eagle_merit_badge(self):
        test_signoff = Signoff.objects.get(
            scout=self.scout, requirement__code="03d", requirement__badge__name="Life"
        )

        self.assertEqual(test_signoff.date, datetime.date(2023, 6, 29))
        self.assertEqual(test_signoff.signed, True)

    def test_allocation_first_aid_merit_badge(self):
        test_signoff = Signoff.objects.get(
            scout=self.scout, requirement__code="03a", requirement__badge__name="Eagle"
        )

        self.assertEqual(test_signoff.date, datetime.date(2023, 6, 30))
        self.assertEqual(test_signoff.signed, True)

    def test_allocation_swimming_merit_badge(self):
        test_signoff = Signoff.objects.get(
            scout=self.scout, requirement__code="03l", requirement__badge__name="Eagle"
        )

        self.assertEqual(test_signoff.date, datetime.date(2022, 7, 1))
        self.assertEqual(test_signoff.signed, True)

    def test_allocation_elective_merit_badge(self):
        test_signoff = Signoff.objects.get(
            scout=self.scout, requirement__code="03o", requirement__badge__name="Eagle"
        )

        self.assertEqual(test_signoff.date, datetime.date(2022, 7, 1))
        self.assertEqual(test_signoff.signed, True)
