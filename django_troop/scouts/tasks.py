import logging

from badges.models import Badge
from celery import shared_task

logger = logging.getLogger(__name__)


@shared_task()
def check_completed_ranks(scout):
    for badge in Badge.objects.filter(badge_type="rank").all():
        if badge.is_complete(scout):
            scout.signoff_completed_badge(badge)
            scout.completed_ranks.add(badge.final)
    scout.save()


@shared_task()
def check_current_rank(scout):
    check_completed_ranks(scout)
    max_rank_index = max(
        [signoff.requirements.badge.order for signoff in scout.completed_ranks]
    )
    scout.current_rank = Badge.objects.filter(
        badge_type="rank", order=max_rank_index + 1
    )
    scout.save()


#
# @shared_task()
# def check_leadership_requirement(scout):
#    if scout.current_rank.order < 3:
#        return # no leadership requirement for current Scout, Tenderfoot, Second Class to reach next rank
#
#    else:
#        requirement = Requirement.objects.get(badge__order=scout.current_rank_order+1, text__icontains="Leadership")
#
#        needed_months = requirement.quantity
#
