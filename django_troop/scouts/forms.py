from django import forms
from scouts.models import User


class ScoutForm(forms.ModelForm):
    user_type = forms.CharField(
        widget=forms.HiddenInput(), initial="scout", required=False
    )

    parents = forms.MultipleChoiceField(
        choices=((None, None)),
        required=False,
    )

    class Meta:
        model = User
        fields = [
            "first_name",
            "last_name",
            "middle_name",
            "nick_name",
            "suffix",
            "age",
            "username",
            "email",
            "patrol",
            "parents",
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["parents"].choices = [
            (adult.pk, adult) for adult in User.adults.all()
        ]


class AdultForm(forms.ModelForm):
    user_type = forms.CharField(
        widget=forms.HiddenInput(), initial="adult", required=False
    )

    children = forms.MultipleChoiceField(choices=((None, None),), required=False)

    class Meta:
        model = User
        fields = [
            "user_type",
            "first_name",
            "last_name",
            "middle_name",
            "nick_name",
            "suffix",
            "username",
            "email",
            "children",
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["children"].choices = [
            (child.pk, child) for child in User.scouts.all()
        ]


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = [
            "user_type",
            "first_name",
            "last_name",
            "middle_name",
            "nick_name",
            "suffix",
            "username",
            "email",
        ]
