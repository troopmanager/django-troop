import logging
import time

from celery import shared_task

logger = logging.getLogger(__name__)


@shared_task()
def placeholder_multiply_numbers(count: int, delay: float):
    print(
        f"multiply numbers starting at 1 and continuing to {count} with a delay of {delay}"
    )
    result = 1
    for n in range(2, count):
        result *= n
        print(f"number is {result} now. Waiting for {delay} seconds")
        time.sleep(delay)
    print(f"done multiplying numbers: result={result}")
    return result
