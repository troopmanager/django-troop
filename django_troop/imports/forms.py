from django import forms


class TroopmasterImportForm(forms.Form):
    input_file = forms.FileField(
        label="Please Select your individual history PDF file",
        required=True,
        allow_empty_file=False,
    )


class YAMLImportForm(forms.Form):
    input_file = forms.FileField(
        label="Please Select your YAML File",
        required=True,
        allow_empty_file=False,
    )


class TroopwebhostImportForm(forms.Form):
    input_scouts_file = forms.FileField(required=True, allow_empty_file=False)
    input_adults_file = forms.FileField(required=False)
    input_signoffs_file = forms.FileField(required=False)
    input_merit_badges_file = forms.FileField(required=False)
    input_merit_badges_partials_file = forms.FileField(required=False)


class ScoutbookImportForm(forms.Form):
    scouts_file = forms.FileField(
        label="Select your scout information file (CSV)", required=False
    )
    advancement_file = forms.FileField(
        label="Select your advancement information file (CSV)", required=True
    )


class PlaceholderImportForm(forms.Form):
    count = forms.IntegerField(
        label="Count of numbers to multiply",
        required=True,
        initial=10,
        min_value=1,
        max_value=100,
    )
    delay_secs = forms.FloatField(
        label="Delay in seconds between each multiply",
        required=True,
        min_value=0.1,
        max_value=100.0,
        initial=10.0,
    )
