import imports.forms as Forms
import yaml
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView
from imports.tasks import placeholder_multiply_numbers
from scoutbook_parser import Parser as SBParser
from scouts.load_scouts import load_scout
from tm_parser import Parser as TMParser
from tqdm import tqdm

from django_troop.celery import app


class ImportView(PermissionRequiredMixin, TemplateView):
    template_name = "imports/import.html"
    permission_required = ("scouts.import_users",)


class YAMLImportView(PermissionRequiredMixin, FormView):
    template_name = "imports/YAML_import.html"
    form_class = Forms.YAMLImportForm
    permission_required = ("scouts.import_users",)

    def form_valid(self, form):
        parsed_scouts = yaml.safe_load(form.cleaned_data["input_file"].file)

        for scout in tqdm(
            parsed_scouts.values(),
            desc="Scouts",
            total=len(parsed_scouts),
        ):
            load_scout.delay(scout)

        messages.success(
            self.request,
            f"Started importing {len(parsed_scouts)} Scouts. Standby . . . ",
        )
        return HttpResponseRedirect(reverse_lazy("scouts_list"))


class TroopmasterImportView(PermissionRequiredMixin, FormView):
    template_name = "imports/troopmaster_import.html"
    form_class = Forms.TroopmasterImportForm
    success_url = reverse_lazy("scouts_list")
    permission_required = ("scouts.import_users",)

    def form_valid(self, form):
        parsed_scouts = TMParser(stream=form.cleaned_data["input_file"].file)

        for scout in tqdm(
            parsed_scouts.scouts.values(),
            desc="Scouts",
            total=len(parsed_scouts.scouts),
        ):
            load_scout.delay(scout)

        messages.success(
            self.request,
            f"Started importing {len(parsed_scouts.scouts)} Scouts. Standby . . . ",
        )
        return HttpResponseRedirect(reverse_lazy("scouts_list"))


class ScoutbookImportView(PermissionRequiredMixin, FormView):
    template_name = "imports/scoutbook_import.html"
    form_class = Forms.ScoutbookImportForm
    success_url = reverse_lazy("scouts_list")
    permission_required = ("scouts.import_users",)

    def form_valid(self, form):
        decoded_personal = (
            form.cleaned_data["scouts_file"].file.read().decode("utf-8").splitlines()
        )
        decoded_advancement = (
            form.cleaned_data["advancement_file"]
            .file.read()
            .decode("utf-8")
            .splitlines()
        )
        parsed_scouts = SBParser(
            personal=decoded_personal, advancement=decoded_advancement
        )

        for scout in tqdm(
            parsed_scouts.scouts.values(),
            desc="Scouts",
            total=len(parsed_scouts.scouts),
        ):
            load_scout.delay(scout)

        messages.success(
            self.request,
            f"Started importing {len(parsed_scouts.scouts)} Scouts. Standby . . . ",
        )
        return HttpResponseRedirect(reverse_lazy("scouts_list"))


class PlaceholderImportView(PermissionRequiredMixin, FormView):
    template_name = "imports/placeholder_import.html"
    form_class = Forms.PlaceholderImportForm
    permission_required = ()
    success_url = reverse_lazy("imports")

    def form_valid(self, form: Forms.PlaceholderImportForm):
        count, delay = form.cleaned_data["count"], form.cleaned_data["delay_secs"]
        placeholder_multiply_numbers.delay(count, delay)
        return super().form_valid(form)


@login_required
def troopmaster_import_success(request):
    return HttpResponse("got file")


@login_required
def scoutbook_import_success(request):
    return HttpResponse("got file")


@login_required
def placeholder_import_success(request):
    return HttpResponse("got file")


@login_required
def placeholder_tasks(request):
    tasks = collect_tasks()
    context = {"tasks": tasks}
    return render(request, "imports/placeholder_partial_tasks.html", context)


def collect_tasks():
    i = app.control.inspect()
    tasks = []
    active = i.active() or {}
    for worker_tasks in active.values():
        tasks.extend(worker_tasks)
    return tasks
