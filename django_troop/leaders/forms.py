from django import forms
from leaders.models import Leader


class LeaderForm(forms.ModelForm):
    class Meta:
        model = Leader
        fields = ["user", "position", "start_date", "end_date", "mentor", "comments"]
