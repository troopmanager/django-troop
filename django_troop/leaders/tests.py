from datetime import date

from django.test import TestCase
from leaders.models import Leader
from positions.models import Position
from scouts.models import User

# Create your tests here.


LEADERS = [
    {
        "start_date": date(2022, 1, 5),
        "end_date": date(2023, 5, 4),
        "user": {
            "username": "janphillips",
            "first_name": "jan",
            "last_name": "phillips",
        },
        "position": {
            "name": "treasurer",
            "position_type": "adult",
        },
        "comments": "did a good job",
        "mentor": {
            "first_name": "bob",
            "last_name": "morris",
            "username": "bmorris",
        },
    },
    {
        "start_date": date(2021, 1, 5),
        "end_date": None,
        "user": {
            "username": "mikephillips",
            "first_name": "mike",
            "last_name": "phillips",
        },
        "position": {
            "name": "quartermaster",
            "position_type": "scout",
        },
        "comments": "did a good job",
        "mentor": {
            "first_name": "Joe",
            "last_name": "Mason",
            "username": "jmason",
        },
    },
]


class LeaderTest(TestCase):
    def setUp(self):
        self.positions = []
        self.users = []
        self.leaders = []
        self.mentors = []

        for index, leader in enumerate(LEADERS):
            self.positions.append(Position.objects.create(**leader["position"]))
            self.users.append(User.objects.create(**leader["user"]))
            self.leaders.append(
                Leader.objects.create(
                    start_date=LEADERS[index]["start_date"],
                    end_date=LEADERS[index]["end_date"],
                    user=self.users[index],
                    position=self.positions[index],
                    comments=LEADERS[index]["comments"],
                )
            )
            if leader["mentor"]:
                self.mentors.append(User.objects.create(**leader["mentor"]))
                self.leaders[index].mentor = self.mentors[index]
                self.leaders[index].save()

    def test_position(self):
        self.assertEqual(self.leaders[0].position.name, "treasurer")
        self.assertEqual(self.leaders[1].position.name, "quartermaster")

    def test_leader_name(self):
        self.assertEqual(self.leaders[0].user.first_name, "jan")
        self.assertEqual(self.leaders[0].user.last_name, "phillips")
        self.assertEqual(self.leaders[1].user.first_name, "mike")
        self.assertEqual(self.leaders[1].user.last_name, "phillips")

    def test_mentor(self):
        self.assertEqual(self.leaders[1].mentor.first_name, "Joe")
        self.assertEqual(self.leaders[1].mentor.last_name, "Mason")
        self.assertEqual(self.leaders[0].mentor.first_name, "bob")
        self.assertEqual(self.leaders[0].mentor.last_name, "morris")

    def test_is_active(self):
        self.assertFalse(self.leaders[0].is_active())
        self.assertTrue(self.leaders[1].is_active())
