from datetime import date

from leaders.models import Leader
from positions.models import Position

POSITION_EQUIVALENTS = {}


def load_leaders(scout, scout_obj):
    for item in scout.get("Leadership", []):
        for title, data in item.items():
            position, created = Position.objects.get_or_create(
                name=title, position_type="scout"
            )
            Leader.objects.create(
                user=scout_obj,
                start_date=data["Start Date"],
                end_date=data["End Date"],
                position=position,
            )


def assign_patrol_leader(scout, patrol, start_date=date.today()):
    patrol.leader = scout
    patrol.save()
    leader = Leader.objects.get_or_create(
        user=scout, position=Position.objects.get(name="Patrol Leader")
    )
    leader.patrol = patrol
    leader.save()
