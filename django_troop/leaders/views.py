from django.contrib import messages
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.db.models import Q
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from leaders.models import Leader
from positions.models import Position

from .forms import LeaderForm


class LeaderListView(LoginRequiredMixin, ListView):
    model = Leader
    context_object_name = "leaders"
    ordering = ["user__last_name"]


class ScoutLeaderListView(LoginRequiredMixin, ListView):
    context_object_name = "leaders"
    ordering = ["user__last_name"]
    queryset = Leader.objects.filter(
        Q(position__position_type="scout") | Q(position__position_type="either"),
        end_date__isnull=True,
    )


class AdultLeaderListView(LoginRequiredMixin, ListView):
    context_object_name = "leaders"
    ordering = ["user__last_name"]
    queryset = Leader.objects.filter(
        Q(position__position_type="adult") | Q(position__position_type="either"),
        end_date__isnull=True,
    )


class LeaderDetailView(LoginRequiredMixin, DetailView):
    model = Leader
    context_object_name = "leader"


class LeaderCreateView(PermissionRequiredMixin, CreateView):
    model = Leader
    fields = ["user", "position", "start_date", "end_date", "mentor", "comments"]
    permission_required = ("leaders.add_leader",)


class LeaderUpdateView(PermissionRequiredMixin, UpdateView):
    model = Leader
    fields = ["user", "position", "start_date", "end_date", "mentor", "comments"]
    permission_required = ("leaders.change_leader",)


class LeaderDeleteView(PermissionRequiredMixin, DeleteView):
    model = Leader
    success_url = reverse_lazy("leader_list")
    permission_required = ("leaders.delete_leader",)


@permission_required("leaders.add_leader")
def leader_create(request, position_pk):
    initial = {
        "position_id": position_pk,
        "position": get_object_or_404(Position, pk=position_pk),
    }

    if request.method == "POST":
        form = LeaderForm(request.POST, initial=initial)

        if form.is_valid():
            updated_leader = form.save()
            messages.success(request, 'Leader "{}" was updated.'.format(updated_leader))

            return redirect("leader_detail", updated_leader.pk)
    form = LeaderForm(initial=initial)

    return render(
        request, "leaders/leader_form.html", {"method": request.method, "form": form}
    )
