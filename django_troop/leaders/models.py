from datetime import date

from django.db import models
from django.urls import reverse_lazy


class Leader(models.Model):
    start_date = models.DateField(default=date.today)
    end_date = models.DateField(null=True, blank=True)
    user = models.ForeignKey(
        "scouts.User", on_delete=models.CASCADE, related_name="leader_positions"
    )
    position = models.ForeignKey(
        "positions.Position", on_delete=models.CASCADE, related_name="leader_scouts"
    )

    comments = models.CharField(max_length=200, null=True, blank=True)
    mentor = models.ForeignKey(
        "scouts.User",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="mentor_positions",
    )
    patrol = models.ForeignKey(
        "patrols.patrol",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="patrol_positions",
    )

    def get_absolute_url(self):
        return reverse_lazy("leader_detail", kwargs={"pk": self.pk})

    def is_active(self):
        if self.end_date and self.end_date <= date.today():
            return False
        return True

    def __str__(self):
        return f"{self.user.first_name}, {self.user.last_name[0]} {self.position.name}"
