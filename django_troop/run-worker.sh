#!/bin/bash

set -exo pipefail

echo "starting celery worker"
celery -A django_troop worker --loglevel=info --concurrency 5 -E --without-mingle
code=$?
sleep 1
echo "celery exited with code $code"
sleep 1
exit $code
