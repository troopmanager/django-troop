from datetime import date

from django.contrib.auth.models import Group
from django.db import models
from django.urls import reverse_lazy
from leaders.models import Leader
from positions.models import Position


class Patrol(models.Model):
    name = models.CharField(max_length=100)

    def get_absolute_url(self):
        return reverse_lazy("patrol_detail", kwargs={"pk": self.pk})

    def get_leader(self):
        ic()  # noqa
        try:
            return Leader.objects.get(
                end_date__isnull=True, patrol=self, position__name="Patrol Leader"
            )
        except Leader.DoesNotExist:
            return None

    def set_leader(self, user):
        ic()  # noqa
        if not user:
            return None
        if patrol_leader := self.get_leader():
            patrol_leader.end_date = date.today()
            patrol_leader.save()
        patrol_leader_position = Position.objects.get(name="Patrol Leader")
        Leader.objects.create(
            start_date=date.today(),
            patrol=self,
            position=patrol_leader_position,
            user=user,
        )

    def get_mentor(self):
        try:
            return Leader.objects.get(
                end_date__isnull=True, patrol=self, position__name="Patrol Mentor"
            )
        except Leader.DoesNotExist:
            return None

    def set_mentor(self, mentor):
        if not mentor:
            return None
        if mentor_leader := self.get_mentor():
            mentor_leader.end_date = date.today()
            mentor_leader.save()
        patrol_mentor_position = Position.objects.get(name="Patrol Mentor")
        Leader.objects.create(
            start_date=date.today(),
            patrol=self,
            position=patrol_mentor_position,
            user=mentor,
        )


class UserGroup(models.Model):
    description = models.CharField(max_length=100)
    group = models.ForeignKey(
        Group, related_name="group_profile", on_delete=models.CASCADE
    )
