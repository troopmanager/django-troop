from django.test import TestCase
from django.urls import reverse
from patrols.models import Patrol
from scouts.models import User

# Create your tests here.


class PatrolTests(TestCase):
    def setUp(self):
        url = reverse("patrol_list")
        user = User.objects.create(user_type="adult", last_name="Jones")
        self.client.force_login(user)
        self.response = self.client.get(url)

    def test_url_exists_at_current_location(self):
        self.assertEqual(self.response.status_code, 200)

    def test_badge_list_template(self):
        self.assertTemplateUsed(self.response, "patrols/patrol_list.html")


class PatrolDetailTests(TestCase):
    def setUp(self):
        self.patrol = Patrol.objects.create(name="Goober Patrol")

    def test_patrol(self):
        self.assertEqual(self.patrol.name, "Goober Patrol")

    def test_add_scout(self):
        self.assertEqual(self.patrol.patrol_members.count(), 0)

        scout = User.scouts.create(
            first_name="Michael", last_name="Hanson", username="MHanson"
        )

        self.patrol.patrol_members.add(scout)

        self.assertEqual(self.patrol.patrol_members.count(), 1)

        scout2 = User.scouts.create(
            first_name="Susie", last_name="Q", username="SusieQ"
        )

        self.patrol.patrol_members.add(scout2)

        self.assertEqual(self.patrol.patrol_members.count(), 2)
