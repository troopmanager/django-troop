from badges.models import Badge
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.db.models import Count, Q
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from patrols.forms import PatrolForm
from patrols.models import Patrol
from requirements.models import Requirement
from scouts.models import User
from signoffs.models import Signoff

TOP_RANK_NEEDS = 5


def set_patrol_members(view, form):
    form.instance.patrol_members.set([])

    for scout in form.cleaned_data.get("patrol_members"):
        form.instance.patrol_members.add(User.objects.get(pk=scout))


def set_patrol_mentor(view, form):
    ic()  # noqa
    if mentor := form.cleaned_data.get("mentor"):
        ic(mentor)  # noqa
        form.instance.mentor(mentor)


def set_patrol_leader(view, form):
    ic()  # noqa
    if leader := form.cleaned_data.get("leader"):
        ic(leader)  # noqa
        form.instance.set_leader(leader)


class PatrolUpdateView(PermissionRequiredMixin, UpdateView):
    model = Patrol
    form_class = PatrolForm
    permission_required = ["patrols.change_patrol"]

    def form_valid(self, form):
        ic()  # noqa
        set_patrol_members(self, form)
        set_patrol_mentor(self, form)
        set_patrol_leader(self, form)
        self.object = form.save()

        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, *args, **kwargs):
        ic()  # noqa
        context = super().get_context_data(*args, **kwargs)
        patrol = self.get_object()
        ic(patrol)  # noqa
        context["form"] = PatrolForm(
            initial={
                "name": patrol.name,
                "patrol_members": [user.id for user in patrol.patrol_members.all()],
                "leader": patrol.get_leader().user.id if patrol.get_leader() else None,
                "mentor": patrol.get_mentor().user.id if patrol.get_mentor() else None,
            }
        )
        return context

    def get_form_kwargs(self):
        ic()  # noqa
        ic(self)  # noqa
        kwargs = super().get_form_kwargs()
        patrol = self.get_object()
        kwargs["initial"] = {
            "patrol_members": [scout.id for scout in patrol.patrol_members.all()],
            "leader": patrol.get_leader().user.id if patrol.get_leader() else [],
            "mentor": patrol.get_mentor().user.id if patrol.get_mentor() else [],
        }
        kwargs["instance"] = patrol

        return kwargs


class PatrolCreateView(PermissionRequiredMixin, CreateView):
    model = Patrol
    form_class = PatrolForm
    permission_required = ("patrols.add_patrol",)

    def form_valid(self, form):
        self.object = form.save()

        set_patrol_members(self, form)
        self.object = form.save()

        return HttpResponseRedirect(self.get_success_url())


class PatrolDeleteView(PermissionRequiredMixin, DeleteView):
    model = Patrol
    success_url = reverse_lazy("patrol_list")
    permission_required = "patrols.change_patrol"


class PatrolListView(LoginRequiredMixin, ListView):
    model = Patrol
    context_object_name = "patrols"
    ordering = "name"


class PatrolDetailView(LoginRequiredMixin, DetailView):
    model = Patrol
    context_object_name = "patrol"
    template_name = "patrols/patrol_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["scouts"] = context["patrol"].patrol_members

        context["mentor"] = context["patrol"].get_mentor()
        context["leader"] = context["patrol"].get_leader()

        context["rank_needs"] = {}

        # take the scouts in this patrol
        # for each rank up to first class
        # and each requirement except those in the "Excludable" group
        # (Scoutmaster Conference, Board of Review, etc)
        # count how many scouts from this patrol still need that requirement
        # sort by count descending and take the TOP_RANK_NEEDS highest for each badge
        #
        for rank in Badge.objects.filter(
            badge_type="rank", order__lte=4, order__gt=0
        ).all():
            context["rank_needs"][rank.order] = (
                Requirement.objects.filter(
                    badge=rank, signoffs__scout__in=context["scouts"].all()
                )
                .exclude(labels__name__contains="Excludable")
                .annotate(count=Count("signoffs__id", filter=Q(signoffs__signed=False)))
                .order_by("-count")
                .exclude(count__lte=0)
                .values(
                    "count",
                    "code",
                    "text",
                    "badge__order",
                    "badge",
                    "badge__id",
                    "badge__name",
                )[:TOP_RANK_NEEDS]
            )

        # scouts to display in patrol advancement table
        context["advancement_scouts"] = (
            context["scouts"]
            .filter(current_rank__order__lte=4)
            .order_by("last_name", "first_name")
        )

        context["signoffs"] = Signoff.objects.filter(
            requirement__badge__badge_type="rank",
            scout__in=context["advancement_scouts"].all(),
            requirement__badge__order__lte=4,
            requirement__badge__order__gte=1,
            scout__current_rank__order__lte=4,
        ).order_by(
            "requirement__badge",
            "requirement__code",
            "scout__last_name",
            "scout__first_name",
        )

        return context
