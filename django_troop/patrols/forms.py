import logging

from django import forms
from patrols.models import Patrol
from scouts.models import User

logger = logging.getLogger(__name__)


class PatrolForm(forms.ModelForm):
    patrol_members = forms.MultipleChoiceField(
        choices=((None, None)),
    )

    leader = forms.ChoiceField(
        choices=((None, None)),
    )

    mentor = forms.ChoiceField(
        choices=((None, None)),
    )

    class Meta:
        model = Patrol
        fields = [
            "name",
            "patrol_members",
            "leader",
            "mentor",
        ]

    def __init__(self, *args, **kwargs):
        ic()  # noqa
        super().__init__(*args, **kwargs)
        ic(kwargs)  # noqa
        if kwargs.get("instance"):
            qm = kwargs.get("instance").patrol_members
        else:
            qm = User.scouts

        self.fields["patrol_members"] = forms.MultipleChoiceField(
            widget=forms.CheckboxSelectMultiple,
            required=False,
            label="Patrol Members",
            choices=self.get_member_choices(),
        )

        self.fields["leader"] = forms.ModelChoiceField(
            required=False,
            queryset=qm,
        )

        self.fields["mentor"] = forms.ModelChoiceField(
            required=False,
            queryset=User.adults,
        )

        logger.info(self.fields["patrol_members"].initial)

    def get_member_choices(self):
        choices = []
        for scout in User.scouts.order_by("patrol", "last_name").all():
            name_str = f"{scout.first_name} {scout.last_name}"
            if scout.patrol:
                name_str += f" ({scout.patrol.name})"
            else:
                name_str += " (Not assigned)"
            choices.append((scout.pk, name_str))
        return choices
