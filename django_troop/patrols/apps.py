from django.apps import AppConfig


class PatrolsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "patrols"
