import patrols.views as views
from django.urls import path

urlpatterns = [
    path("", views.PatrolListView.as_view(), name="patrol_list"),
    path("<int:pk>", views.PatrolDetailView.as_view(), name="patrol_detail"),
    path("update/<int:pk>/", views.PatrolUpdateView.as_view(), name="patrol_update"),
    path("new/", views.PatrolCreateView.as_view(), name="patrol_create"),
    path("delete/<int:pk>/", views.PatrolDeleteView.as_view(), name="patrol_delete"),
]
