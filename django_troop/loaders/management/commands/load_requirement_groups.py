import logging
from pathlib import Path

import toml
from badges.models import Badge
from django.conf import settings
from django.core.management.base import BaseCommand
from requirement_labels.models import RequirementLabel
from requirements.models import Requirement
from tqdm import tqdm

DATA_DIR = settings.DATA_DIR

logger = logging.getLogger(__name__)


def load_requirement_groups(path):
    data = toml.load(path)
    for name, group_data in tqdm(data.items(), desc="Requirement Groups"):
        if group_data["type"] == "rank":
            group, created = RequirementLabel.objects.get_or_create(name=name)
            if created:
                group.save()
            fill_rank_requirement_group(group, name, group_data)
        elif group_data["type"] == "merit_badge":
            group, created = RequirementLabel.objects.get_or_create(name=name)
            if created:
                group.save()
            fill_merit_badge_requirement_group(group, name, group_data)

    logging.info("Loading requirement groups complete")


def fill_rank_requirement_group(group, name, data):
    for rank_name, requirement_list in tqdm(
        data["requirements"].items(), desc=group.name
    ):
        r = Badge.objects.get(badge_type="rank", name=rank_name)
        for requirement_code in tqdm(requirement_list, desc=rank_name):
            req = Requirement.objects.get(badge=r, code=requirement_code)
            req.labels.add(group)
    req.save()


def fill_merit_badge_requirement_group(group, name, group_data):
    ...


class Command(BaseCommand):
    help = "Loads requirement groups"

    def add_arguments(self, parser):
        parser.add_argument("config_dir", nargs="?", default=DATA_DIR, type=str)
        parser.add_argument("--load_type", nargs="?", default="scoutbook", type=str)

    def handle(self, *args, **kwargs):
        DATA_DIR = Path(kwargs["config_dir"])

        load_requirement_groups(DATA_DIR / "requirement_groups.toml")


def main(*args, **kwargs):
    Command().handle(config_dir=DATA_DIR, **kwargs)
