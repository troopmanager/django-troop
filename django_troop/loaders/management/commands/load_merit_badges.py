import csv
from datetime import datetime
from pathlib import Path

from badges.models import Badge, get_mb_name
from django.conf import settings
from django.core.management.base import BaseCommand
from requirements.models import Requirement
from scouts.models import User
from signoffs.models import Signoff
from tm_parser import Parser
from tqdm import tqdm

DATA_DIR = settings.DATA_DIR
REMOVE_PERIOD = str.maketrans("", "", ".")


def load_merit_badges(path, load_type):
    match load_type:
        case "troopwebhost":
            with open(path / "merit_badges.csv", encoding="utf-8-sig") as f:
                reader = csv.DictReader(f)
                for line in tqdm(reader, desc="Merit Badge Completions"):
                    load_troopwebhost_merit_badges(line)

        case "troopmaster":
            p = Parser(path / "tm_data.pdf")
            for scout in tqdm(
                p.scouts.values(), desc="Scouts-signoffs", total=len(p.scouts.values())
            ):
                load_troopmaster_merit_badges(scout)


def load_troopwebhost_merit_badges(line):
    if line["Earned"]:
        merit_badge_name = get_mb_name(line["Merit Badge"])
        badge = Badge.objects.get(type="merit_badge", name=merit_badge_name)
        name = User.name_from_string(line["Scout"])
        scout = User.scouts.get(
            last_name=name.get("last_name"),
            first_name=name.get("first_name"),
        )
        requirement = Requirement.objects.get(badge=badge, code="Completion")
        signoff = Signoff.objects.get(scout=scout, requirement=requirement)

        signoff.date = datetime.strptime(line["Earned"], "%m/%d/%y").date()
        signoff.signed = True

        signoff.save()


def load_troopmaster_merit_badges(scout):
    pass


class Command(BaseCommand):
    help = "Loads Merit Badges"

    def add_arguments(self, parser):
        parser.add_argument("config_dir", nargs="?", default=DATA_DIR, type=str)

    def handle(self, *args, **kwargs):
        DATA_DIR = Path(kwargs["config_dir"])

        load_merit_badges(DATA_DIR, load_type=kwargs["load_type"])


def main(*args, **kwargs):
    Command().handle(config_dir=DATA_DIR, **kwargs)
