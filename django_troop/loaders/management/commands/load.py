import toml
from badges.load_ranks import main as load_ranks
from django.conf import settings
from django.core.management.base import BaseCommand
from merit_badges.load_merit_badges import main as load_merit_badges
from merit_badges.load_scout_completions import main as load_merit_badge_completions
from patrols.load_patrols import main as load_patrols
from positions.load_positions import main as load_positions
from scouts.load_adults import main as load_adults
from scouts.load_scouts import main as load_scouts
from signoffs.load_signoffs import main as load_signoffs

from .load_requirement_groups import main as load_requirement_groups

DATA_DIR = settings.DATA_DIR

local_config = toml.load(DATA_DIR / "local_settings.toml")


class Command(BaseCommand):
    help = "Loads ranks, requirements, positions, scouts, patrols, and signoffs"

    def add_arguments(self, parser):
        parser.add_argument("base_dir", nargs="?", default=DATA_DIR, type=str)
        parser.add_argument("load_type", nargs="?", default="scoutbook", type=str)

    def handle(self, *args, **kwargs):
        load_type = local_config.get(
            "IMPORT_TYPE", kwargs.get("load_type", "scoutbook")
        )
        load_ranks(load_type=load_type)
        load_merit_badges(load_type=load_type)
        load_positions(load_type=load_type)
        load_requirement_groups(load_type=load_type)
        load_patrols(load_type=load_type)
        load_scouts(load_type=load_type)
        load_adults(load_type=load_type)
        load_merit_badge_completions(load_type=load_type)
        load_signoffs(load_type=load_type)
