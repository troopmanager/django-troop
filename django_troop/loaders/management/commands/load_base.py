from badges.load_ranks import main as load_ranks
from django.conf import settings
from django.core.management.base import BaseCommand
from merit_badges.load_merit_badges import main as load_merit_badges
from positions.load_positions import main as load_positions

from .load_permissions import main as load_permissions
from .load_requirement_groups import main as load_requirement_groups

DATA_DIR = settings.DATA_DIR

# local_config = toml.load(DATA_DIR / "local_settings.toml")


class Command(BaseCommand):
    help = "Loads ranks, requirements, positions, merit_badges"

    def add_arguments(self, parser):
        parser.add_argument("base_dir", nargs="?", default=DATA_DIR, type=str)
        parser.add_argument("load_type", nargs="?", default="scoutbook", type=str)

    def handle(self, *args, **kwargs):
        load_type = "scoutbook"
        load_ranks(load_type=load_type)
        load_merit_badges(load_type=load_type)
        load_positions(load_type=load_type)
        load_requirement_groups(load_type=load_type)
        load_permissions(load_type=load_type)
