from pathlib import Path

import toml

data = toml.load(Path("..") / "troops" / "example_troop" / "requirements.toml")

print(data)
