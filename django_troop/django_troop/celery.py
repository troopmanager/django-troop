import os

from celery import Celery
from django.conf import settings

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "django_troop.settings")

app = Celery("django_troop")

app.config_from_object("django.conf:settings", namespace="CELERY")
app.conf.result_backend_transport_options = {
    "global_keyprefix": f"{settings.TROOP}_",
}
app.conf.broker_transport_options = {"global_keyprefix": f"{settings.TROOP}_"}

app.autodiscover_tasks()
