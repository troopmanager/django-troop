from django.core.cache.backends.redis import RedisCache


class CustomRedisCache(RedisCache):
    def validate_key(self, key):
        """Custom validation, raising exceptions or warnings as needed."""
        ...
