"""
URL configuration for django_troop project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path("", include("scouts.urls")),
    path("admin/", admin.site.urls),
    path("imports/", include("imports.urls")),
    path("accounts/", include("allauth.urls")),
    path("scouts/", include("scouts.urls")),
    path("adults/", include("scouts.adult_urls")),
    path("users/", include("scouts.user_urls")),
    path("patrols/", include("patrols.urls")),
    path("positions/", include("positions.urls")),
    path("leaders/", include("leaders.urls")),
    path("badges/", include("badges.urls")),
    path("requirement_labels/", include("requirement_labels.urls")),
    path("signoffs/", include("signoffs.urls")),
    path("site_admin/", include("site_admin.urls")),
    path("invitations/", include("invitations.urls", namespace="invitations")),
    path("health/", include("health_check.urls")),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    import debug_toolbar

    urlpatterns = [
        path("__debug__/", include(debug_toolbar.urls)),
    ] + urlpatterns


# accounts/login/ [name='login']
# accounts/logout/ [name='logout']
# accounts/password_change/ [name='password_change']
# accounts/password_change/done/ [name='password_change_done']
# accounts/password_reset/ [name='password_reset']
# accounts/password_reset/done/ [name='password_reset_done']
# accounts/reset/<uidb64>/<token>/ [name='password_reset_confirm']
# accounts/reset/done/ [name='password_reset_complete']
