from django.urls import path

from . import views

urlpatterns = [
    path("", views.RequirementLabelListView.as_view(), name="requirement_labels_list"),
    path(
        "<int:pk>",
        views.RequirementLabelDetailView.as_view(),
        name="requirement_labels_detail",
    ),
    path(
        "edit/<int:pk>/",
        views.RequirementLabelUpdateView.as_view(),
        name="requirement_labels_update",
    ),
    path(
        "edit/new/",
        views.RequirementLabelCreateView.as_view(),
        name="requirement_labels_create",
    ),
    path(
        "edit/delete/<int:pk>/",
        views.RequirementLabelDeleteView.as_view(),
        name="requirement_labels_delete",
    ),
]
