from django.apps import AppConfig


class RequirementLabelsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "requirement_labels"
