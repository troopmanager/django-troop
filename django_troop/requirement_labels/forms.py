from django import forms
from requirement_labels.models import RequirementLabel
from requirements.models import Requirement


class RequirementLabelForm(forms.ModelForm):
    requirements = forms.MultipleChoiceField(
        choices=((None, None)),
    )

    class Meta:
        model = RequirementLabel
        fields = [
            "name",
            "requirements",
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["requirements"] = forms.MultipleChoiceField(
            widget=forms.CheckboxSelectMultiple,
            required=False,
            label="Requirements",
            choices=self.get_choices(),
        )

    def get_choices(self):
        choices = (
            (requirement.pk, f"{requirement.badge.name} {requirement.code}")
            for requirement in Requirement.objects.filter(badge__badge_type="rank")
            .order_by("badge", "code")
            .all()
        )
        return choices
