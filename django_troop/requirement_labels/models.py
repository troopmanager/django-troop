from django.db import models


class RequirementLabel(models.Model):
    name = models.CharField(max_length=100)
