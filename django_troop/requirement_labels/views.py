from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from requirement_labels.forms import RequirementLabelForm
from requirement_labels.models import RequirementLabel
from signoffs.models import Signoff


class RequirementLabelListView(LoginRequiredMixin, ListView):
    model = RequirementLabel
    context_object_name = "labels"
    template_name = "requirement_labels/labels_list.html"


class RequirementLabelDetailView(LoginRequiredMixin, DetailView):
    model = RequirementLabel
    context_object_name = "label"
    template_name = "requirement_labels/requirement_label_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["signoffs"] = Signoff.objects.filter(
            requirement__in=context["label"].requirements.all()
        )
        return context


class RequirementLabelUpdateView(PermissionRequiredMixin, UpdateView):
    model = RequirementLabel
    form_class = RequirementLabelForm
    permission_required = ["requirement_labels.change_requirement_label"]

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["form"] = RequirementLabelForm(
            initial={
                "name": self.get_object().name,
                "requirements": [
                    requirement.id
                    for requirement in self.get_object().requirements.all()
                ],
            }
        )
        return context


class RequirementLabelCreateView(PermissionRequiredMixin, CreateView):
    model = RequirementLabel
    template_name = "requirement_labels/label_update.html"
    form_class = RequirementLabelForm
    permission_required = "requirement_labels.add_requirement_label"
    success_url = reverse_lazy("requirement_labels_list")


class RequirementLabelDeleteView(PermissionRequiredMixin, DeleteView):
    model = RequirementLabel
    success_url = reverse_lazy("requirement_labels_list")
    permission_required = "requirement_labels.delete_requirement_label"
    template_name = "requirement_labels/label_confirm_delete.html"
