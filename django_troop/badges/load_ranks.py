import logging
from pathlib import Path

import toml
from badges.models import Badge
from django.conf import settings
from django.core.management.base import BaseCommand
from requirements.models import Requirement
from tqdm import tqdm

DATA_DIR = settings.DATA_DIR


logger = logging.getLogger(__name__)


def load_ranks(path, load_type):
    data = toml.load(path)

    for rank, rank_data in tqdm(data.items(), desc="Ranks", total=len(data.items())):
        Badge.objects.filter(name=rank, badge_type="rank")
        r, created = Badge.objects.get_or_create(
            name=rank,
            badge_type="rank",
        )
        if created:
            logger.info("creating rank")
            r.order = rank_data["rank_order"]
            r.save()

        final_code = rank_data["final_code"]
        for code, more_data in tqdm(rank_data["requirements"].items(), desc=rank):
            req, created = Requirement.objects.get_or_create(
                badge=r,
                code=code,
                text=more_data["text"],
                full_text=more_data["full_text"],
                equivalent_requirements=more_data.get("equivalent_requirements"),
            )
            if created:
                req.save()
                logger.info("creating requirements")

            if code == final_code:
                r.final = req
            if q := more_data.get("quantity"):
                req.quantity = q
            if q_type := more_data.get("type"):
                req.quantity_type = q_type
            req.save()
        r.save()
    logger.warning("Loading Ranks Complete")


class Command(BaseCommand):
    help = "Loads ranks and requirements"

    def add_arguments(self, parser):
        parser.add_argument("config_dir", nargs="?", default=DATA_DIR, type=str)
        parser.add_argument("--load_type", nargs="?", default="scoutbook", type=str)

    def handle(self, *args, **kwargs):
        DATA_DIR = Path(kwargs["config_dir"])
        # local_config = toml.load(DATA_DIR / "local_settings.toml")
        load_type = kwargs.get("load_type", None)

        load_ranks(DATA_DIR / "requirements.toml", load_type=load_type)


def main(*args, **kwargs):
    Command().handle(config_dir=DATA_DIR, **kwargs)
