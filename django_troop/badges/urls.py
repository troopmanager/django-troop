from django.urls import path

from . import views

urlpatterns = [
    path("", views.RankListView.as_view(), name="rank_list"),
    path("<int:pk>", views.RankDetailView.as_view(), name="rank_detail"),
]
