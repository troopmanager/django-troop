import logging
import re

import toml
from django.db import models
from requirements.models import Requirement
from signoffs.models import Signoff

logger = logging.getLogger(__name__)

ALTERNATE_NAMES = toml.load("data/common_files/mb_equivalent_names.toml")


class Badge(models.Model):
    name = models.CharField(max_length=100)

    badge_type = models.CharField(
        max_length=50,
        choices=(("rank", "rank"), ("merit_badge", "merit_badge"), ("award", "award")),
    )

    def __str__(self):
        return f"{self.badge_type} {self.name}"

    order = models.SmallIntegerField(null=True, blank=True)
    final = models.ForeignKey(
        "requirements.Requirement",
        null=True,
        on_delete=models.SET_NULL,
        related_name="final_requirement",
        blank=True,
    )

    eagle_required = models.BooleanField(default=False)
    leadership_required = models.IntegerField(null=True, blank=True)
    service_required = models.IntegerField(null=True, blank=True)
    conservation_service_required = models.IntegerField(null=True, blank=True)

    class Meta:
        permissions = [
            ("assign_merit_badge", "Can assign a new merit badge for a scout to begin"),
        ]

    def __lt__(self, other):
        if self.order and self.order < other.order:
            return True
        return False

    def is_complete(self, scout):
        print(f"is-complete called with {self}, {scout}")
        try:
            badge_completed = Signoff.final_signoffs.filter(
                requirement__badge=self, scout=scout, signed=True
            ).exists()
        except Signoff.DoesNotExist:
            pass

        all_count = Requirement.objects.filter(badge=self).count()

        signed_count = (
            Signoff.signed_requirements.filter(requirement__badge=self)
            .filter(scout=scout)
            .count()
        )

        return badge_completed or all_count == signed_count

    def start_user(self, user):
        logger.warning(f"in badge.start {self.name} for {user.full_name}")
        for requirement in self.requirements.all():
            signoff, created = Signoff.objects.get_or_create(
                scout=user, requirement=requirement
            )
            if created:
                logger.warning(
                    f"created signoff {signoff.requirement.code} {signoff.requirement.badge.name}"
                )
            else:
                logger.warning(
                    f"already had signoff {signoff.requirement.code} {signoff.requirement.badge.name}"
                )

    def finish_user(self, user):
        """Records all un-signed requirements as being signed (without adding date)
        for a rank badge, starts the next rank badge"""
        logger.warning(f"in badge.finish_user {self.name} for {user.full_name}")

        signoffs = Signoff.unsigned_requirements.filter(
            requirement__badge=self, scout=user
        ).all()
        for signoff in signoffs:
            logger.warning(
                f"signing {signoff.requirement.code} from badge {signoff.requirement.badge.name}"
            )
            signoff.signed = True
            signoff.save()
        if (
            self.badge_type == "rank"
            and user.current_rank
            and self.order > user.current_rank.order
        ):
            user.current_rank = self
            if user.current_rank.order < 7:  # Eagle
                next_badge = Badge.objects.get(
                    order=user.current_rank.order + 1, badge_type="rank"
                )
                next_badge.start_user(user)
        user.save()


def get_mb_name(line):
    pat = re.compile(r"(.*) \(.*\)\*?")
    match = pat.match(line.strip())
    if match:
        mb_name = match.group(1)
    else:
        mb_name = line.strip(r"*")

    if mb_name in ALTERNATE_NAMES:
        mb_name = ALTERNATE_NAMES[mb_name]

    return mb_name
