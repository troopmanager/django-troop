import os

import yaml
from django.core.management.base import BaseCommand
from dotenv import dotenv_values
from scouts.load_scouts import load_scout

ENVIRON = {
    **dotenv_values(),
    **dotenv_values(".env.example"),
    **os.environ,
}


class Command(BaseCommand):
    help = "Creates an admin user non-interactively if it doesn't exist"

    def add_arguments(self, parser):
        parser.add_argument("--input-file", help="Input YAML file")

    def handle(self, *args, **options):
        with open(options["input_file"]) as f:
            scouts = yaml.safe_load(f)
            for scout in scouts:
                load_scout.delay(scout)
