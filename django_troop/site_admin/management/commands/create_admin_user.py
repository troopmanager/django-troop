import os

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand
from dotenv import dotenv_values

ENVIRON = {
    **dotenv_values(),
    **dotenv_values(".env.example"),
    **os.environ,
}


class Command(BaseCommand):
    help = "Creates a non-super-user admin user non-interactively if it doesn't exist"

    def add_arguments(self, parser):
        parser.add_argument("--username", help="User's username")
        parser.add_argument("--email", help="User's email")
        parser.add_argument("--password", help="User's password")

    def handle(self, *args, **options):
        User = get_user_model()

        if not User.objects.filter(username=options["username"]).exists():
            group = Group.objects.get(name="Adult Leaders")
            u = User.objects.create_user(
                username=options["username"],
                email=options["email"],
                password=options["password"],
            )

            u.groups.add(group)
            u.save()
