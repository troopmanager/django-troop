from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.models import Group
from django.urls import reverse_lazy
from django.views.generic import CreateView, DeleteView, UpdateView
from django.views.generic.edit import FormView
from django.views.generic.list import ListView
from scouts.models import User

from .forms import GroupUpdateForm, InviteForm, UserUpdateForm


class AdminUserListView(PermissionRequiredMixin, ListView):
    template_name = "site_admin/users_list.html"
    permission_required = "scouts.change_user"
    queryset = User.objects


class AdminUserUpdateView(PermissionRequiredMixin, UpdateView):
    model = User
    context_object_name = "user"
    template_name = "site_admin/users_update.html"
    form_class = UserUpdateForm
    permission_required = "scouts.change_user"
    success_url = reverse_lazy("site_admin.user_list")

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["form"] = UserUpdateForm(
            initial={
                "groups": [group.id for group in self.get_object().groups.all()],
                "username": self.get_object().username,
                "user_type": self.get_object().user_type,
                "email": self.get_object().email,
                "is_active": self.get_object().is_active,
            }
        )
        return context


class AdminGroupListView(PermissionRequiredMixin, ListView):
    model = Group
    context_object_name = "groups"
    template_name = "site_admin/group_list.html"
    permission_required = ["group.change_group", "group.add_group"]


class AdminGroupUpdateView(PermissionRequiredMixin, UpdateView):
    model = Group
    context_object_name = "group"
    template_name = "site_admin/group_update.html"
    form_class = GroupUpdateForm
    permission_required = "group.change_group"
    success_url = reverse_lazy("site_admin.group_list")

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["form"] = GroupUpdateForm(
            initial={
                "name": self.get_object().name,
                "users": [user.id for user in self.get_object().user_set.all()],
                "permissions": [
                    permission.pk for permission in self.get_object().permissions.all()
                ],
            }
        )
        return context


class AdminGroupCreateView(PermissionRequiredMixin, CreateView):
    model = Group
    template_name = "site_admin/group_update.html"
    form_class = GroupUpdateForm
    permission_required = "group.add_group"
    success_url = reverse_lazy("site_admin.group_list")


class AdminGroupDeleteView(PermissionRequiredMixin, DeleteView):
    model = Group
    success_url = reverse_lazy("site_admin.group_list")
    permission_required = "group.change_group"
    template_name = "site_admin/group_confirm_delete.html"


class AdminInviteView(PermissionRequiredMixin, FormView):
    form_class = InviteForm
    template_name = "site_admin/invite_user.html"
    permission_required = "invitation.add_invitation"
