from django import forms
from django.contrib.auth.models import Group, Permission
from patrols.models import Patrol
from scouts.models import User

EXCLUDE_PERMISSIONS = [
    "account",
    "admin",
    "auth",
    "contenttypes",
    "sessions",
    "socialaccount",
]


class InviteForm(forms.ModelForm):

    email = forms.EmailField()
    first_name = forms.CharField(required=False)
    last_name = forms.CharField(required=False)
    middle_name = forms.CharField(required=False)
    nick_name = forms.CharField(required=False)
    suffix = forms.CharField(required=False)
    patrol = forms.ChoiceField(required=False)
    username = forms.CharField(required=False)

    class Meta:
        model = User
        fields = [
            "user_type",
            "first_name",
            "last_name",
            "middle_name",
            "nick_name",
            "suffix",
            "username",
            "email",
            "patrol",
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["patrol"].choices = [
            (patrol.pk, patrol) for patrol in Patrol.objects.all()
        ]


class UserUpdateForm(forms.ModelForm):
    groups = forms.MultipleChoiceField(choices=((None, None)))

    class Meta:
        model = User
        fields = [
            "user_type",
            "username",
            "email",
            "groups",
            "is_active",
        ]

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        self.fields["groups"] = forms.MultipleChoiceField(
            label="Groups",
            choices=self.get_group_choices(),
        )

    def get_group_choices(self):
        return ((group.pk, group.name) for group in Group.objects.all())


class GroupUpdateForm(forms.ModelForm):
    users = forms.ModelMultipleChoiceField(queryset=User.objects.all(), required=False)
    permissions = forms.MultipleChoiceField(choices=((None, None)), required=False)

    class Meta:
        model = Group
        fields = [
            "name",
            "users",
            "permissions",
        ]

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        self.fields["permissions"] = forms.MultipleChoiceField(
            label="User Permissions",
            widget=forms.CheckboxSelectMultiple,
            choices=self.get_permissions_choices(),
            required=False,
        )

        self.fields["users"] = forms.MultipleChoiceField(
            label="Select Users",
            choices=self.get_users_choices(),
            required=False,
        )

    def get_permissions_choices(self):
        return (
            (permission.pk, permission)
            for permission in Permission.objects.exclude(name__icontains="view")
            .exclude(content_type__app_label__in=EXCLUDE_PERMISSIONS)
            .all()
        )

    def get_users_choices(self):
        return ((user.pk, user) for user in User.objects.all())
