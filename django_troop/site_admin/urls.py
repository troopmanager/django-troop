from django.urls import path

from . import views

urlpatterns = [
    path(
        "users/",
        views.AdminUserListView.as_view(),
        name="site_admin.user_list",
    ),
    path(
        "users/<int:pk>/",
        views.AdminUserUpdateView.as_view(),
        name="site_admin.user_update",
    ),
    path(
        "groups/",
        views.AdminGroupListView.as_view(),
        name="site_admin.group_list",
    ),
    path(
        "groups/<int:pk>/",
        views.AdminGroupUpdateView.as_view(),
        name="site_admin.group_update",
    ),
    path(
        "groups/new/",
        views.AdminGroupCreateView.as_view(),
        name="site_admin.group_create",
    ),
    path(
        "groups/delete/<int:pk>/",
        views.AdminGroupDeleteView.as_view(),
        name="site_admin.group_delete",
    ),
    path("invite/", views.AdminInviteView.as_view(), name="site_admin.invite"),
]
