from django.contrib import messages
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import get_object_or_404, redirect, render
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView
from django.views.generic.list import ListView

from .forms import PositionForm
from .models import Position


class PositionListView(LoginRequiredMixin, ListView):
    model = Position
    context_object_name = "positions"
    template_name = "positions/positions_list.html"
    ordering = ["name"]


class PositionDetailView(LoginRequiredMixin, DetailView):
    model = Position
    context_object_name = "position"
    template_name = "positions/position_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context["incumbents"] = (
            context["position"].leader_scouts.filter(end_date__isnull=True).all()
        )

        context["veterans"] = (
            context["position"].leader_scouts.filter(end_date__isnull=False).all()
        )

        return context


class PositionUpdateView(PermissionRequiredMixin, UpdateView):
    model = Position
    template_name = "positions/position_update.html"
    fields = ["name", "description", "position_type"]
    permission_required = "positions.change_position"


@permission_required("positions.change_position")
def position_edit(request, pk=None):
    if pk is not None:
        position = get_object_or_404(Position, pk=pk)
    else:
        position = None

    if request.method == "POST":
        form = PositionForm(request.POST, instance=position)
        if form.is_valid():
            updated_position = form.save()
            if position is None:
                messages.success(
                    request, 'Position "{}" was created.'.format(updated_position)
                )
            else:
                messages.success(
                    request, 'Position "{}" was updated.'.format(updated_position)
                )

            return redirect("position_edit", updated_position.pk)
    else:
        form = PositionForm(instance=position)

    return render(
        request, "positions/form-example.html", {"method": request.method, "form": form}
    )
