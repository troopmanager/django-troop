from django.db import models
from django.urls import reverse_lazy


class Position(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=200, null=True, blank=True)
    position_type = models.CharField(
        max_length=10,
        null=True,
        blank=True,
        choices=(("scout", "scout"), ("adult", "adult"), ("either", "either")),
    )

    patrol_related = models.BooleanField(null=True, blank=True)

    def __str__(self):
        return f"{self.name}"

    def get_absolute_url(self):
        return reverse_lazy("position_detail", kwargs={"pk": self.pk})
