import logging
from pathlib import Path

import toml
from django.conf import settings
from django.core.management.base import BaseCommand
from positions.models import Position
from tqdm import tqdm

DATA_DIR = settings.DATA_DIR

logger = logging.getLogger(__name__)


def load_positions(path):
    data = toml.load(path)
    for name, position_list in tqdm(data.items(), desc="Positions"):
        logger.info(name, position_list)
        position_type = (
            "scout" if "Scout" in name else "adult" if "Adult" in name else "either"
        )
        logger.info(position_type)
        for item in position_list:
            position, created = Position.objects.get_or_create(
                name=item, position_type=position_type
            )


class Command(BaseCommand):
    help = "Loads Position definitions"

    def add_arguments(self, parser):
        parser.add_argument("config_dir", nargs="?", default=DATA_DIR, type=str)
        parser.add_argument("--load_type", nargs="?", default="scoutbook", type=str)

    def handle(self, *args, **kwargs):
        DATA_DIR = Path(kwargs["config_dir"])

        load_positions(DATA_DIR / "positions.toml")


def main(*args, **kwargs):
    Command().handle(config_dir=DATA_DIR, **kwargs)
