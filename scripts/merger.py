import sys

import deepmerge
import yaml

merger = deepmerge.merger.Merger(
    [
        (dict, "merge"),
        (list, "override"),
        (set, "intersect"),
    ],
    ["override"],
    ["override"],
)


def merge_files(*files):
    output = {}
    for file in files:
        try:
            with open(file) as f:
                new_config = yaml.safe_load(f)
            output = merger.merge(output, new_config)
        except FileNotFoundError:
            print(
                f"Skipping provided file which did not exist: {file}", file=sys.stderr
            )
        except yaml.YAMLError:
            print(f"Error: provided file has invalid yaml: {file}", file=sys.stderr)
            sys.exit(1)
    return output


if __name__ == "__main__":
    print(yaml.safe_dump(merge_files(*sys.argv[1:])))
