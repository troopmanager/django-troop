#!/usr/bin/env python3

import difflib
import shutil

from merger import merge_files


def update_hosts():
    """
    Reads troop names from the config file and adds entries to the hosts file if
    they don't exist.
    """

    config_files = ["config-default.yaml", "config.yaml", "config-troops.yaml"]
    hosts_file = "/etc/hosts"
    backup_file = "/etc/hosts.bak"
    suffix = "managed by django-troop"

    # print(f"Back up {hosts_file} to {backup_file}")
    shutil.copy2(hosts_file, backup_file)

    config = merge_files(*config_files)

    # print(f"Read {hosts_file} and determine the existing troop entries")
    lines_before = []
    with open(hosts_file, "r") as f:
        lines_before = [line for line in f]
        other_hosts = [line for line in lines_before if suffix not in line]

    hostname = config["infra"]["hostname"]
    destination_server = config["infra"]["destinationServer"]
    troops = config["troops"]

    # print(f"Determine the desired troop entries from the config")
    troop_hosts = []
    for troop in troops:
        troop_name = troop["name"]
        entry = f"{destination_server} {troop_name}.{hostname} ; {suffix}\n"
        # if entry in old_troop_hosts:
        #   print(f"Keep existing entry: {entry.strip()}")
        # else:
        #   print(f"Add entry for {troop_name}")
        troop_hosts.append(entry)

    # Check for differences.
    lines_after = other_hosts + troop_hosts
    content_after = "".join(lines_after)
    differ = difflib.Differ()
    diff = list(differ.compare(lines_before, lines_after))
    if not any(line.startswith("+") or line.startswith("-") for line in diff):
        print("No changes detected. Hosts file already up to date.")
        return

    # Show the user a diff and let them decide whether they like it.
    print(f"Here is the resulting diff for {hosts_file}:")
    print()
    print("".join(diff))
    print()

    confirm = input("Do you want to proceed? (yes/no): ")
    if confirm.lower() != "yes":
        print("Aborted without changing.")
        return

    # Update the hosts file with the existing hosts at the top and the
    # django-troops entries following.
    print(f"Updating {hosts_file}:")
    with open(hosts_file, "w") as f:
        f.write(content_after)

    print("Done!")


if __name__ == "__main__":
    update_hosts()
