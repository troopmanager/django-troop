#!/usr/bin/env bash

set -exo pipefail

if [[ $# -ne 4 ]]; then
	echo "wrong number of arguments"
	echo "args: TROOP PORT USER EMAIL"
	exit 1
fi


DEBUG=False
TROOP=$1
PORT=$2
USER=$3
EMAIL=$4
export DJANGO_SUPERUSER_PASSWORD=$(tr -dc A-Za-z0-9 </dev/urandom | head -c 13; echo)

mkdir -p troops/$TROOP
cd troops/$TROOP/
ln -sf ../../data/common_files/* .

cat << END_OF_ENV > .env
TROOP=${TROOP}
DJANGO_ALLOWED_HOSTS=${TROOP}.troopmanager.org localhost 127.0.0.1 [::1]
PORT=${PORT}
END_OF_ENV

cat > nginx.conf << END_OF_NGINX
server {
	server_name ${TROOP}.troopmanager.org;
	location / {
		include proxy_params;
		proxy_pass http://127.0.0.1:${PORT};
	}
}

server {
    if (\$host = ${TROOP}.troopmanager.org) {
        return 301 https://\$host\$request_uri;
    } # managed by Certbot


	listen 80;
	server_name ${TROOP}.troopmanager.org;
    return 404; # managed by Certbot

}
END_OF_NGINX

sudo cp nginx.conf /etc/nginx/conf.d/${TROOP}.nginx.conf
sudo certbot -n --nginx -d ${TROOP}.troopmanager.org

sudo systemctl reload nginx

touch ${TROOP}-db.sqlite3

cd $BASEDIR
docker-compose -p ${TROOP} --env-file troops/${TROOP}/.env up -d --build

echo "Sleeping for 10 seconds…"
sleep 10
echo "Completed"

docker-compose -p $TROOP --env-file troops/${TROOP}/.env exec web python manage.py createsuperuser --noinput --username ${USER} --email ${EMAIL}
