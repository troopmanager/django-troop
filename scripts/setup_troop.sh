#!/usr/bin/env bash

# strict mode for bash
set -exo pipefail

# make sure we have the right number of
if [[ $# -ne 2 ]]; then
	echo "wrong number of arguments"
	echo "args: TROOP PORT"
	exit 1
fi

DEBUG=False
TROOP=$1
PORT=$2

BASEDIR=$(pwd)

mkdir -p troops/$TROOP/nginx/conf.d
cd troops/$TROOP/
cp -r ../../data/common_files/* .
touch ${TROOP}-db.sqlite3

cat << END_OF_ENV > .env
TROOP=${TROOP}
DJANGO_ALLOWED_HOSTS=${TROOP}.troopmanager.org localhost 127.0.0.1 [::1]
PORT=${PORT}
END_OF_ENV


cd $BASEDIR
