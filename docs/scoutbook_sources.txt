
consider using the scoutbook_parser package:

$ pip install scoutbook_parser

from scoutbook_parser.parser import Parser
p = Parser(input_advancement="<filename>", input_personal="<filename>", file_format="json")

for scout, data in p.scouts.items():
    # do something with scout and data


scoutbook export

advancement
bsa member id - store as string in scouts table
first name - store as string in scouts table
middle name - store as string in scouts table
last name - store as string in scouts table
advancement type - create a Badge (rank, merit badge or award) item of this type, and create Requirements items for each requirement
advancement - this is the name of the item to be created. If the advancement type is "Rank" then appropriate requirements should be recorded as Requirements and linked to this rank.
version - ignore
date completed (mm/dd/yyyy) - store this item as a Signoff item joining the Scout item to the Badge item
approved bool - ignore
awarded bool - ignore
marked completed by (name) - ignore
marked completed date - ignore
counselor approved by (name) - ignore
counselor approved date - ignore
leader approved by (name) - ignore
leader approved date - ignore
awarded by (name) - ignore
awarded date - ignore


personal_data.csv
userid int - ignore
bsa member id int - store in Scout table
first name name - store in Scout table
last name name - store in Scout table
suffix name - store in Scout table
nickname name - store in Scout table
address 1 str - store in Scout table
address 2 str - store in Scout table
city str - store in Scout table
state str  - store in Scout table
zip str - store in Scout table
home phone str - store in Scout table
school name str - store in Scout table
lds - ignore
swimming classification - ignore
swimming classification date yyyy-mm-dd - ignore
unit number str - ignore
unit type troop/pack/crew etc - ignore
date joined scouts bsa yyyy-mm-dd - ignore
den type - ignore
den number - ignore
date joined den yyyy-mm-dd - ignore
patrol name str - ensure there is a Patrol named this in the Group table, then add this to the groups in the Scout table
date joined patrol yyyy-mm-dd - ignore
parent 1 email str - ignore
parent 2 email str - ignore
parent 3 email str - ignore
oa member number int - ignore
oa election date yyyy-mm-dd - ignore
oa ordeal date yyyy-mm-dd - ignore
oa brotherhood date yyyy-mm-dd - ignore
oa vigil date yyyy-mm-dd - ignore
oa active yes/no - ignore


scoutbook parsed

BSA ID
  Address 1
  Address 2
  Adventures
    Adventure
      Approved bool
      Awarded bool
      AwardedBy name
      AwardedDate date
      CounselorApprovedBy
      CounselorApprovedDate date
      Date
      Date completed
      LeaderApprovedBy
      LeaderApprovedDate
      MarkedCompletedBy
      MarkedCompletedDate
  Award Requirements
    Award
      requirement code
	      Approved bool
	      Awarded bool
	      AwardedBy name
	      AwardedDate date
	      CounselorApprovedBy
	      CounselorApprovedDate date
	      Date
	      Date completed
	      LeaderApprovedBy
	      LeaderApprovedDate
	      MarkedCompletedBy
	      MarkedCompletedDate
  Awards
    Award
      Approved bool
      Awarded bool
      AwardedBy name
      AwardedDate date
      CounselorApprovedBy
      CounselorApprovedDate date
      Date
      Date completed
      LeaderApprovedBy
      LeaderApprovedDate
      MarkedCompletedBy
      MarkedCompletedDate

  BSA ID
  City
  Date Joined Den
  Date Joined Patrol
  Date Joined Scouts Bsa
  Den Number
  Den Type
  First Name
  Home Phone
  LDS
  Last Name
  Merit Badges
    Badge
      Approved bool
      Awarded bool
      AwardedBy name
      AwardedDate date
      CounselorApprovedBy
      CounselorApprovedDate date
      Date
      Date completed
      LeaderApprovedBy
      LeaderApprovedDate
      MarkedCompletedBy
      MarkedCompletedDate
  Nickname
  OA Active
  OA Brotherhood Date
  OA Election Date
  OA Member Number
  OA Ordeal Date
  OA Vigil Date
  Parent 1 Email
  Parent 2 Email
  Parent 3 Email
  Patrol Name
  Rank Requirements
    Rank
      Requirement
	      Approved bool
	      Awarded bool
	      AwardedBy name
	      AwardedDate date
	      CounselorApprovedBy
	      CounselorApprovedDate date
	      Date
	      Date completed
	      LeaderApprovedBy
	      LeaderApprovedDate
	      MarkedCompletedBy
	      MarkedCompletedDate
  User ID
  Suffix
  State
  Zip
  School Grade
  School Name
  Swimming Classification
  Swimming Classification Date
  Unit Number
  Unit Type
  Webelos Adventure Requirements

  Merit Badge Requirments
    Badge
      Requirement
	      Approved bool
	      Awarded bool
	      AwardedBy name
	      AwardedDate date
	      CounselorApprovedBy
	      CounselorApprovedDate date
	      Date
	      Date completed
	      LeaderApprovedBy
	      LeaderApprovedDate
	      MarkedCompletedBy
	      MarkedCompletedDate
