# Import tilt extensions.
# Docs: https://docs.tilt.dev/extensions.html
load('ext://configmap', 'configmap_from_dict')
load('ext://helm_resource', 'helm_repo', 'helm_resource')
load('ext://namespace', 'namespace_create')
load('./tilt/config/Tiltfile', 'init_config')

# Increase the default timeout since cert-manager can take longer than 30s.
allow_k8s_contexts('default')
update_settings (k8s_upsert_timeout_secs = 120)

# Load defaults from 'config-default.yaml' and overrides from 'config.yaml'.
#conf = init_config('config-default.yaml', 'config.yaml')

conf = decode_yaml(local('python scripts/merger.py config-default.yaml config.yaml config-troops.yaml'))


# Set the default registry for images. This is needed for Kubernetes, since
# nodes need to pull from an external registry.
registry = conf["infra"]["imageRegistry"]
print('Setting default registry to', registry)
default_registry(registry)


if conf["infra"]["deployIngressNginx"]:
    # Install ingress-nginx for kind, which implements the Kubernetes ingress using
    # nginx. Note that Kubernetes itself does not provide an implementation for
    # ingress, which is why we need to install it separately.
    #
    # Kubernetes ingress: https://kubernetes.io/docs/concepts/services-networking/ingress/
    # Kind ingress guide: https://kind.sigs.k8s.io/docs/user/ingress/
    ingress_nginx_url='https://raw.githubusercontent.com/kubernetes/ingress-nginx/{}/deploy/static/provider/kind/deploy.yaml'.format(conf['infra']['ingressNginxVersion'])
    print('Loading ingress-nginx from url:', ingress_nginx_url)
    ingress_nginx_yaml=local(
        'curl {}'.format(ingress_nginx_url),
        echo_off=True,
        quiet=True,
    )
    k8s_yaml(ingress_nginx_yaml)
    k8s_resource(
        'ingress-nginx-controller',
        labels='infra',
        links='http://{}/'.format(conf["infra"]["hostname"]),
        # Wait for the jobs that create a secret which is needed by the controller.
        resource_deps=[
            'ingress-nginx-admission-create',
            'ingress-nginx-admission-patch',
        ],
    )
    k8s_resource(
        'ingress-nginx-admission-create',
        labels='misc',
    )
    k8s_resource(
        'ingress-nginx-admission-patch',
        labels='misc',
    )

# Install redis as needed for celery.
# Chart README: https://github.com/bitnami/charts/tree/main/bitnami/redis/
helm_resource(
    "redis",
    "oci://registry-1.docker.io/bitnamicharts/redis",
    labels='infra',
    namespace='redis',
    flags=[
        "--create-namespace",
        "--set=architecture=standalone",
        # TODO: add authentication, prob with certs. Definitely don't port
        # forward this or expose it in any way until it has encryption and auth.
        "--set=auth.enabled=false",
        "--set=master.disableCommands=",
        '--version={}'.format(conf['infra']['redisVersion']),
        '--debug',
    ],
)

# Install cert-manager for managing TLS certs.
# Chart README: https://cert-manager.io/docs/installation/helm/
# Options: https://artifacthub.io/packages/helm/cert-manager/cert-manager
helm_repo('jetstack', 'https://charts.jetstack.io', labels='misc')
helm_resource(
    'cert-manager',
    'jetstack/cert-manager',
    namespace='cert-manager',
    labels='infra',
    flags=[
        '--create-namespace',
        '--set=crds.enabled=true',
        '--version={}'.format(conf['infra']['certManagerVersion']),
        '--debug',
    ],
    resource_deps=['jetstack'],
)

# Deploy our app.

# Deply core resources needed by all troops.
namespace_create('troopmanager')
tmpl = helm(
    'deploy/helm/core',
    namespace='troopmanager',
)
k8s_yaml(tmpl)
k8s_resource(
    new_name='core-troopmanager',
    labels='misc',
    objects=[
        'selfsigned-issuer:clusterissuer',
    ],
    resource_deps=['cert-manager'],
)

docker_build(
    'django-troop',
    'django_troop',
    dockerfile='django_troop/Dockerfile',
)

# Each troop is deployed in its own namespace with a special hostname and
# ingress. See the tilt resource page for links to the home page via DNS record
# or via direct port forwarding.
for troop in conf["troops"]:
    name = troop["name"]
    ns = "troop-{}".format(name)
    hostname = '{}.{}'.format(name, conf["infra"]["hostname"])
    link = 'http://{}/'.format(hostname)

    print('Deploy troop {} in namespace {} at {}'.format(name, ns, link))
    namespace_create(ns)

    # Generate configmaps for the base and troop vars with a name based on the
    # hash. This ensures that the pod will be restarted when the config changes.
    app_env = conf['appEnv']
    troop_env = troop.get('env', {})
    base_cm_name = 'django-troop-base-config-{}'.format(hash(str(app_env)))
    troop_cm_name = 'django-troop-troop-config-{}'.format(hash(str(troop_env)))
    k8s_yaml(configmap_from_dict(base_cm_name, namespace=ns, inputs=app_env))
    k8s_yaml(configmap_from_dict(troop_cm_name, namespace=ns, inputs=troop_env))

    tmpl = helm(
        'deploy/helm/troop',
        namespace=ns,
        set=[
            'troopName={}'.format(name),
            'hostname={}'.format(hostname),
            'ingressPort={}'.format(conf["infra"]["ingressPort"]),
            'redisAddr={}'.format(conf["appEnv"]["REDIS_URL"]),
            'sqliteDatabaseFolder={}/{}'.format(conf["infra"]["sqliteDatabaseBasePath"], name),
            'baseConfigMap={}'.format(base_cm_name),
            'troopConfigMap={}'.format(troop_cm_name),
        ],
    )
    k8s_yaml(tmpl)
    k8s_resource(
        'django-troop:deployment:{}'.format(ns),
        new_name='{}-django-troop'.format(name),
        labels=['troopmanager'],
        # Forward the troop app directly, for development without having to
        # apply the host entries.
        port_forwards=port_forward(
            local_port=troop["forwardPort"],
            container_port=8000,
            name='port forward',
        ),
        # This will only work if you have applied the host entries.
        links='http://{}/'.format(hostname),
        resource_deps=['redis'],
    )

    resource_deps = []
    if conf['infra']['deployIngressNginx']:
        # We need to wait for the controller to be available because it runs
        # an admission webhook for ingress objects and they will be rejected
        # unless it is running.
        resource_deps.append("ingress-nginx-controller")

    k8s_resource(
        new_name='{}-ingress'.format(name),
        labels=['misc'],
        objects=[
            'troop:ingress:{}'.format(ns),
        ],
        resource_deps=resource_deps
    )
