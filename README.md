# django-Troop

Troop Management Software in Django-Python

An open source web app for managing a Boy Scouts of America Troop

## Principles

Open Source, Self-Hosting if desired, Troop Administration can easily
export/retrieve all data. Clean, modern interface. Flexibility to let scouts do
any tasks adults can do if desired. Plan is to include advancement, leadership,
events, registration, and roster at first.

## Installation

This procedure will install and run an instance of Troopmanager locally for development and testing

1. Install dependencies.

   - [docker](https://docs.docker.com/engine/install/)
   - [just](https://just.systems/man/en/prerequisites.html)

1. Clone the repo

   ```shell
   git clone git://git@gitlab.com/troopmanager/django-troop.git
   ```

1. Update the config with your own values.
    ```shell
    edit django_troop/env.example
    ```

1. Create and run the project.
    ```
    just up -d
   ```

1. Access the project with a web browser on localhost:8000
    The default username/password is admin/admin

1. Creating a virtual environment is recommended:
    ```shell
    python -m venv .venv
    source .venv/bin/activate (in windows, .venv/Scripts/Activate.bat)
    python -m pip install -r requirements.txt -r django_troop/requirements.txt
    ```

## Importing data

### Troopwebhost

1. Gather files and zip them into a zip archive:
   - Scout Directory xxxxxxxxxxx.CSV: obtain from menu->members->troop directory->scout directory
     ("Open in excel" is download as csv)
   - Adult Directory xxxxxxxxxxx.CSV: obtain from menu->members->troop directory->adult directory
     ("Open in excel" is download as csv)
   - Rank Requirements Status xxxxxxxxxxxxx.CSV: obtain from menu->advancement->maintain advancement->export
     rank requirement status to excel
   - Merit Badge History By Scout By Date Earned xxxxxxxxxx.CSV: obtain from menu->advancement->advancement status
     reports->merit badge history by scout by date earned
   - Uncompleted Merit Badge Requirements For Merit Badges In Progress xxxxxxxxxxx.CSV: obtain from menu->advancement->requirement
     reports->uncompleted merit badge requirements by scout

1. Create and activate a virtual environment (see above)

1. Install twh_parser:
    ```shell
    python -m pip install twh_parser
    ```

1. Obtain a YAML file from your data:
    ```shell
    twhparse -z (your .zip filename) > scouts_data.yaml
    or in windows, twhparse -z (your .zip filename) -o scouts_data.yaml
    ```

1. Once you have logged into the system, go to import -> YAML and use the scouts_data.yaml file

### Troopmaster

1. Gather the file from Troopmaster:
    - tm-data.pdf: From Reports->Advancement->Individual History, select all
      scouts, and turn on all options except ["Omit details on completed ranks",
      "Include Start/Last Progress", "Include Counselor", "Include Partial MB
      Remarks", "Mic-O-Say", "Advancement Remarks", "Include Signature Block"]

1. As an alternative, you can use tm_parser to create a YAML file from your data

    - Create and activate a virtual environment (see above)

    ```shell
    python -m pip install tm_parser
    tmparse (your tm-data.pdf file) -o scouts_data.yaml
    ```

    - Then you can use import -> YAML with your scouts_data.yaml file. (this is useful if you would like to inspect or adjust any data before importing (YAML can be edited with a text editor)


1. Once you have logged into the system, go to import -> Troopmaster and use the tm_data.pdf file


### Scoutbook

1. From Scoutbook, obtain the export files:
    - personal.csv: From the troop view, go to Export/Backup, then select "Scouts/Members"
    - advancement.csv: From the troop view, go to Export/Backup, then select "Advancement"

1. As an alternative, you can use scoutbook_parser to create a YAML file from your data

    - Create and activate a virtual environment (see above)

    ```shell
    python -m pip install scoutbook_parser
    sbparse -p (your personal.csv file) (your advancement file) -o scouts_data.yaml
    ```

    - Then you can use import -> YAML with your scouts_data.yaml file. (this is useful if you would like to inspect or adjust any data before importing (YAML can be edited with a text editor)

OR

1. Once you have logged into the system, go to import -> Scoutbook and select your the personal.csv file and advancement.csv file

## Importing

1. When you tell Troopmanager to import scouts, you'll be taken back to the main scout list view. A message will appear with the number of scouts that are to be exported. This exporting happens in the background. You can see information about the pending import jobs by returning to the import page.
